﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Sappi_Kpi_ScoreBoard
{
    public partial class SiteMaster : MasterPage
    {
        Classes.Checks Checks = new Classes.Checks();
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                SetAccessLevels();
            }
        }

        protected void SetAccessLevels()
        {
            switch (Checks.checkAccessLevel())
            {
                case 0:
                    lnkAuditLog.Visible = false;
                    lnkBuildScoreBoard.Visible = false;
                    lnkUserAccess.Visible = false;
                   // Response.Redirect("Display_KpiScoreBoard.aspx");
                    break;

                case 1:
                    lnkAuditLog.Visible = true;
                    lnkBuildScoreBoard.Visible = true;
                    lnkUserAccess.Visible = true;
                    break;

                case 2:
                    lnkAuditLog.Visible = false;
                    lnkBuildScoreBoard.Visible = false;
                    lnkUserAccess.Visible = false;
                   //Response.Redirect("Display_KpiScoreBoard.aspx");
                    break;

                default:            
                    lnkAuditLog.Visible = false;
                    lnkBuildScoreBoard.Visible = false;
                    lnkUserAccess.Visible = false;
                   // Response.Redirect("Display_KpiScoreBoard.aspx");
                    break;
            }

        }
    }
}