﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" MaintainScrollPositionOnPostBack = "true" CodeBehind="BuildDashBoard_CRUD_KpiScoreBoard.aspx.cs" Inherits="Sappi_Kpi_ScoreBoard.Webpages.BuildDashBoard_CRUD_KpiScoreBoard" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">


    <script src="../Scripts/jquery-2.2.3.min.js"></script>
    <script src="../Scripts/jquery-ui-1.12.1.js"></script>
    <link href="../Content/themes/base/jquery-ui.min.css" rel="stylesheet" />

    <link href="../Content/IconSizes.css" rel="stylesheet" />
    <link href="../Content/GridviewTable.css" rel="stylesheet" />   
    <link href="../Content/GridViewReport.css" rel="stylesheet" />
    <link href="../Content/GridviewScroll.css" rel="stylesheet" />
    <script src="../Scripts/GridviewScroll.js"></script> 
   
       
    <table >
        
        <tr>
            <td><h1>Create and Edit Group Names</h1></td>
        </tr>
        <tr>
            <td>
     <%-- <asp:Panel ID="Panel1"  ScrollBars="Auto" Height="300px" Width="1200" runat="server"> --%>      
    <asp:GridView ID="gvDashBoardGroups" CssClass="mytable" runat="server" OnSelectedIndexChanged="gvDashBoardGroups_SelectedIndexChanged" AutoGenerateColumns="False"  Width="1352px"     
      ShowFooter="true" DataKeyNames="Group_ID" OnRowCommand="gvDashBoardGroups_RowCommand" OnRowEditing="gvDashBoardGroups_RowEditing" OnRowCancelingEdit="gvDashBoardGroups_RowCancelingEdit" OnRowUpdating="gvDashBoardGroups_RowUpdating" ShowHeaderWhenEmpty="true" OnRowDataBound="gvDashBoardGroups_RowDataBound" >
   
       
    
    <Columns>
        <asp:TemplateField HeaderText="Select Row">
            <ItemTemplate>
                <asp:LinkButton ID="lnkSelect" runat="server" CommandArgument='<%# Eval("group_id") + ";" + Eval("Group_name")  %>' cssClass="glyphicon glyphicon-play" ToolTip="Select Sub sections to edit" OnClick="lnkSelect_Click"></asp:LinkButton>
            </ItemTemplate>
        </asp:TemplateField>

        <asp:TemplateField HeaderText="Group Name">
            <ItemTemplate>
                <asp:Label Text='<%# Eval("Group_name") %>' runat="server" Font-Bold="true" />
            </ItemTemplate>
            <EditItemTemplate>
                <asp:TextBox ID="txtGroupName" Text='<%# Eval("Group_Name") %>' runat="server" />
            </EditItemTemplate>
            <FooterTemplate>
                <asp:TextBox ID="txtGroupNameFooter" Text="*"  runat="server"/>
<%--               <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ControlToValidate="txtGroupName" ValidateEmptyText="false" runat="server" ErrorMessage="Group name cannot be blank"></asp:RegularExpressionValidator>--%>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="txtGroupNameFooter"  runat="server" ForeColor="Red" ErrorMessage="*" ValidateRequestMode="Disabled"></asp:RequiredFieldValidator>
                </FooterTemplate>
        </asp:TemplateField>


        <asp:TemplateField HeaderText="Year">
            <ItemTemplate>
                <asp:Label Text='<%# Eval("Group_Year") %>' runat="server" Font-Bold="true" />
            </ItemTemplate>
            <EditItemTemplate>
                <%--<asp:TextBox ID="txtGroupYear"  Text='<%# Eval("Group_Year") %>' runat="server" />--%>
                   <asp:DropDownList ID="txtGroupYear" runat="server" SelectedValue='<%# Eval("Group_Year") %>'>
                    <asp:ListItem>2020</asp:ListItem>
                    <asp:ListItem>2021</asp:ListItem>
                    <asp:ListItem>2022</asp:ListItem>
                    <asp:ListItem>2023</asp:ListItem>
                    <asp:ListItem>2024</asp:ListItem>
                    <asp:ListItem>2025</asp:ListItem>
                    <asp:ListItem>2026</asp:ListItem>
                    <asp:ListItem>2027</asp:ListItem>
                    <asp:ListItem>2028</asp:ListItem>
                    <asp:ListItem>2029</asp:ListItem>
                    <asp:ListItem>2030</asp:ListItem>
                    <asp:ListItem>2031</asp:ListItem>
                    <asp:ListItem>2032</asp:ListItem>
                    <asp:ListItem>2033</asp:ListItem>
                    <asp:ListItem>2034</asp:ListItem>
                    <asp:ListItem>2035</asp:ListItem>
                    <asp:ListItem>2036</asp:ListItem>
                    <asp:ListItem>2037</asp:ListItem>
                    <asp:ListItem>2038</asp:ListItem>
                    <asp:ListItem>2039</asp:ListItem>
                    <asp:ListItem>2040</asp:ListItem>
                    <asp:ListItem>2041</asp:ListItem>
                    <asp:ListItem>2042</asp:ListItem>
                    <asp:ListItem>2043</asp:ListItem>
                    <asp:ListItem>2044</asp:ListItem>
                    <asp:ListItem>2045</asp:ListItem>
                    <asp:ListItem>2046</asp:ListItem>
                    <asp:ListItem>2047</asp:ListItem>
                    <asp:ListItem>2048</asp:ListItem>
                    <asp:ListItem>2049</asp:ListItem>
                    <asp:ListItem>2050</asp:ListItem>
                    <asp:ListItem>2051</asp:ListItem>
                    <asp:ListItem>2052</asp:ListItem>
                    <asp:ListItem>2053</asp:ListItem>
                    <asp:ListItem>2054</asp:ListItem>
                    <asp:ListItem>2055</asp:ListItem>
                    <asp:ListItem>2056</asp:ListItem>
                    <asp:ListItem>2057</asp:ListItem>
                    <asp:ListItem>2058</asp:ListItem>
                    <asp:ListItem>2059</asp:ListItem>
                    <asp:ListItem>2060</asp:ListItem>
                    <asp:ListItem>2061</asp:ListItem>
                    <asp:ListItem>2062</asp:ListItem>
                    <asp:ListItem>2063</asp:ListItem>
                    <asp:ListItem>2064</asp:ListItem>
                    <asp:ListItem>2065</asp:ListItem>
                    <asp:ListItem>2066</asp:ListItem>
                    <asp:ListItem>2067</asp:ListItem>
                    <asp:ListItem>2068</asp:ListItem>
                    <asp:ListItem>2069</asp:ListItem>
                    <asp:ListItem>2070</asp:ListItem>
                    <asp:ListItem>2071</asp:ListItem>
                    
        </asp:DropDownList>
            </EditItemTemplate>
            <FooterTemplate>
                <%--<asp:TextBox ID="txtGroupYearFooter"  runat="server" />--%>
                <asp:DropDownList ID="txtGroupYearFooter" runat="server">
                    <asp:ListItem>2020</asp:ListItem>
                    <asp:ListItem>2021</asp:ListItem>
                    <asp:ListItem>2022</asp:ListItem>
                    <asp:ListItem>2023</asp:ListItem>
                    <asp:ListItem>2024</asp:ListItem>
                    <asp:ListItem>2025</asp:ListItem>
                    <asp:ListItem>2026</asp:ListItem>
                    <asp:ListItem>2027</asp:ListItem>
                    <asp:ListItem>2028</asp:ListItem>
                    <asp:ListItem>2029</asp:ListItem>
                    <asp:ListItem>2030</asp:ListItem>
                    <asp:ListItem>2031</asp:ListItem>
                    <asp:ListItem>2032</asp:ListItem>
                    <asp:ListItem>2033</asp:ListItem>
                    <asp:ListItem>2034</asp:ListItem>
                    <asp:ListItem>2035</asp:ListItem>
                    <asp:ListItem>2036</asp:ListItem>
                    <asp:ListItem>2037</asp:ListItem>
                    <asp:ListItem>2038</asp:ListItem>
                    <asp:ListItem>2039</asp:ListItem>
                    <asp:ListItem>2040</asp:ListItem>
                    <asp:ListItem>2041</asp:ListItem>
                    <asp:ListItem>2042</asp:ListItem>
                    <asp:ListItem>2043</asp:ListItem>
                    <asp:ListItem>2044</asp:ListItem>
                    <asp:ListItem>2045</asp:ListItem>
                    <asp:ListItem>2046</asp:ListItem>
                    <asp:ListItem>2047</asp:ListItem>
                    <asp:ListItem>2048</asp:ListItem>
                    <asp:ListItem>2049</asp:ListItem>
                    <asp:ListItem>2050</asp:ListItem>
                    <asp:ListItem>2051</asp:ListItem>
                    <asp:ListItem>2052</asp:ListItem>
                    <asp:ListItem>2053</asp:ListItem>
                    <asp:ListItem>2054</asp:ListItem>
                    <asp:ListItem>2055</asp:ListItem>
                    <asp:ListItem>2056</asp:ListItem>
                    <asp:ListItem>2057</asp:ListItem>
                    <asp:ListItem>2058</asp:ListItem>
                    <asp:ListItem>2059</asp:ListItem>
                    <asp:ListItem>2060</asp:ListItem>
                    <asp:ListItem>2061</asp:ListItem>
                    <asp:ListItem>2062</asp:ListItem>
                    <asp:ListItem>2063</asp:ListItem>
                    <asp:ListItem>2064</asp:ListItem>
                    <asp:ListItem>2065</asp:ListItem>
                    <asp:ListItem>2066</asp:ListItem>
                    <asp:ListItem>2067</asp:ListItem>
                    <asp:ListItem>2068</asp:ListItem>
                    <asp:ListItem>2069</asp:ListItem>
                    <asp:ListItem>2070</asp:ListItem>
                    <asp:ListItem>2071</asp:ListItem>
                    
        </asp:DropDownList>
            </FooterTemplate>
        </asp:TemplateField>


        <asp:TemplateField HeaderText="Set Display Order">
            <ItemTemplate>
                <asp:Label Text='<%# Eval("Setorder") %>' runat="server" Font-Bold="true" />
            </ItemTemplate>
            <EditItemTemplate>
                <%--<asp:TextBox ID="txtSetOrder" Text='<%# Eval("Setorder") %>' runat="server" />--%>
                 <asp:DropDownList ID="txtSetOrder" SelectedValue='<%# Eval("Setorder") %>' runat="server">
                     <asp:ListItem>1</asp:ListItem>
                     <asp:ListItem>2</asp:ListItem>
                     <asp:ListItem>3</asp:ListItem>
                     <asp:ListItem>4</asp:ListItem>
                     <asp:ListItem>5</asp:ListItem>
                     <asp:ListItem>6</asp:ListItem>
                     <asp:ListItem>7</asp:ListItem>
                     <asp:ListItem>8</asp:ListItem>
                     <asp:ListItem>9</asp:ListItem>
                     <asp:ListItem>10</asp:ListItem>
                     <asp:ListItem>11</asp:ListItem>
                     <asp:ListItem>12</asp:ListItem>
                     <asp:ListItem>13</asp:ListItem>
                     <asp:ListItem>14</asp:ListItem>
                     <asp:ListItem>15</asp:ListItem>
                     <asp:ListItem>16</asp:ListItem>
                     <asp:ListItem>17</asp:ListItem>
                     <asp:ListItem>18</asp:ListItem>
                     <asp:ListItem>19</asp:ListItem>
                     <asp:ListItem>20</asp:ListItem>
                     <asp:ListItem>21</asp:ListItem>
                     <asp:ListItem>22</asp:ListItem>
                     <asp:ListItem>23</asp:ListItem>
                     <asp:ListItem>24</asp:ListItem>
                     <asp:ListItem>25</asp:ListItem>
                     <asp:ListItem>26</asp:ListItem>
                     <asp:ListItem>27</asp:ListItem>
                     <asp:ListItem>28</asp:ListItem>
                     <asp:ListItem>29</asp:ListItem>
                     <asp:ListItem>30</asp:ListItem>
                     <asp:ListItem>31</asp:ListItem>
                     <asp:ListItem>32</asp:ListItem>
                     <asp:ListItem>33</asp:ListItem>
                     <asp:ListItem>35</asp:ListItem>
                     <asp:ListItem>36</asp:ListItem>
                     <asp:ListItem>37</asp:ListItem>
                     <asp:ListItem>38</asp:ListItem>
                     <asp:ListItem>39</asp:ListItem>
                     <asp:ListItem>40</asp:ListItem>
                 </asp:DropDownList>
            </EditItemTemplate>
            <FooterTemplate>
                <%--<asp:TextBox ID="txtSetOrderFooter"  runat="server" />--%>
                 <asp:DropDownList ID="txtSetOrderFooter" runat="server">
                     <asp:ListItem>1</asp:ListItem>
                     <asp:ListItem>2</asp:ListItem>
                     <asp:ListItem>3</asp:ListItem>
                     <asp:ListItem>4</asp:ListItem>
                     <asp:ListItem>5</asp:ListItem>
                     <asp:ListItem>6</asp:ListItem>
                     <asp:ListItem>7</asp:ListItem>
                     <asp:ListItem>8</asp:ListItem>
                     <asp:ListItem>9</asp:ListItem>
                     <asp:ListItem>10</asp:ListItem>
                     <asp:ListItem>11</asp:ListItem>
                     <asp:ListItem>12</asp:ListItem>
                     <asp:ListItem>13</asp:ListItem>
                     <asp:ListItem>14</asp:ListItem>
                     <asp:ListItem>15</asp:ListItem>
                     <asp:ListItem>16</asp:ListItem>
                     <asp:ListItem>17</asp:ListItem>
                     <asp:ListItem>18</asp:ListItem>
                     <asp:ListItem>19</asp:ListItem>
                     <asp:ListItem>20</asp:ListItem>
                     <asp:ListItem>21</asp:ListItem>
                     <asp:ListItem>22</asp:ListItem>
                     <asp:ListItem>23</asp:ListItem>
                     <asp:ListItem>24</asp:ListItem>
                     <asp:ListItem>25</asp:ListItem>
                     <asp:ListItem>26</asp:ListItem>
                     <asp:ListItem>27</asp:ListItem>
                     <asp:ListItem>28</asp:ListItem>
                     <asp:ListItem>29</asp:ListItem>
                     <asp:ListItem>30</asp:ListItem>
                     <asp:ListItem>31</asp:ListItem>
                     <asp:ListItem>32</asp:ListItem>
                     <asp:ListItem>33</asp:ListItem>
                     <asp:ListItem>35</asp:ListItem>
                     <asp:ListItem>36</asp:ListItem>
                     <asp:ListItem>37</asp:ListItem>
                     <asp:ListItem>38</asp:ListItem>
                     <asp:ListItem>39</asp:ListItem>
                     <asp:ListItem>40</asp:ListItem>
                 </asp:DropDownList>
            </FooterTemplate>
        </asp:TemplateField>


        <asp:TemplateField HeaderText="Display">
            <ItemTemplate>
                <asp:Label Text='<%# Eval("Display") %>' runat="server" Font-Bold="true" />
            </ItemTemplate>
            <EditItemTemplate>
                <%--<asp:TextBox ID="txtDisplay" Text='<%# Eval("Display") %>' runat="server" />--%>
                  <asp:DropDownList ID="txtDisplay" SelectedValue='<%# Eval("Display").ToString().Trim().ToUpper() %>'   runat="server">
                     <asp:ListItem>Y</asp:ListItem>
                     <asp:ListItem>N</asp:ListItem>
                      </asp:DropDownList>
            </EditItemTemplate>
            <FooterTemplate>
                <%--<asp:TextBox ID="txtDisplayFooter"  runat="server" />--%>
                 <asp:DropDownList ID="txtDisplayFooter" runat="server">
                     <asp:ListItem>Y</asp:ListItem>
                     <asp:ListItem>N</asp:ListItem>
                      </asp:DropDownList>
            </FooterTemplate>
        </asp:TemplateField>

          


        <asp:TemplateField>
            <ItemTemplate>
             <%--<asp:ImageButton ImageUrl=" " runat="server" CommandName="Edit" ToolTip ="Edit"           
            --%>
                <asp:LinkButton runat="server"  CommandName="Edit" CssClass="glyphicon glyphicon-pencil" ToolTip ="Edit Entry"></asp:LinkButton>
                <%--<asp:LinkButton runat="server"  CommandName="delete" ToolTip ="Edit">delete</asp:LinkButton>--%>
            </ItemTemplate>
            <EditItemTemplate>
                 <asp:LinkButton runat="server"  CommandName="Update" CssClass="glyphicon glyphicon-floppy-disk" ToolTip ="Update entry" ></asp:LinkButton>
                <asp:LinkButton runat="server"  CommandName="Cancel" CssClass="glyphicon glyphicon-remove" ToolTip ="Cancel Edit Mode"></asp:LinkButton>
            </EditItemTemplate>
            <FooterTemplate>
                 <asp:LinkButton runat="server"  CommandName="AddNew" CssClass="glyphicon glyphicon-plus" ToolTip ="Add New" ></asp:LinkButton>
            </FooterTemplate>

        </asp:TemplateField>

    </Columns>

        </asp:GridView>
    <%--</asp:Panel>--%>
    <br />
    <asp:Label ID="lblSuccessMessage" runat="server" Text="" ForeColor="Green"></asp:Label>
    <asp:Label ID="lblError" runat="server" Text="" ForeColor="Red"></asp:Label>

                <br />

                </td>
            </tr>
        </table>


      <table>
        <tr>
            <td ><h1>Create and Edit KPI Details </h1></td>

            <td >&nbsp;</td>

        </tr>
          <tr>
              <td style="width: 907px"><h3> <asp:Label ID="lblGroupNameDetails" runat="server" Text="Group ID" ForeColor="Red"> </asp:Label> 
                  <asp:Label ID="lblGroupID" runat="server" Enabled="False" Text="Group ID" Visible="False"></asp:Label>
                  <asp:TextBox ID="txtGroupID" runat="server" Enabled="False" Text="0" Visible="False"></asp:TextBox>
                  </h3> </td>
              <td style="width: 907px">&nbsp;</td>
          </tr>
        <tr>
            <td >
                <%-- <asp:Panel ID="Panel2"  ScrollBars="Auto" Height="300px" Width="1200" runat="server">  --%>
    <asp:GridView ID="gvKPI" runat="server"  AutoGenerateColumns="False"
      ShowFooter="True" DataKeyNames="Kpi_id" OnRowCommand="gvKPI_RowCommand" OnRowEditing="gvKPI_RowEditing" OnRowCancelingEdit="gvKPI_RowCancelingEdit" OnRowUpdating="gvKPI_RowUpdating" ShowHeaderWhenEmpty="True" Width="1352px" >
   
    
    <Columns>

       <%--objective, kpi, resp, uom, target, setorder, display, month_or_year--%>

        <asp:TemplateField HeaderText="Objective">
            <ItemTemplate>
                <asp:Label Text='<%# Eval("objective") %>' runat="server"  Font-Bold="true"/>
            </ItemTemplate>
            <EditItemTemplate>
                <asp:TextBox ID="txtObjective" Text='<%# Eval("objective") %>' runat="server" />
            </EditItemTemplate>
            <FooterTemplate>
                <asp:TextBox ID="txtObjectiveFooter"  runat="server" />
            </FooterTemplate>
        </asp:TemplateField>

         <asp:TemplateField HeaderText="RESP">
            <ItemTemplate>
                <asp:Label Text='<%# Eval("resp") %>' runat="server" Font-Bold="true" />
            </ItemTemplate>
            <EditItemTemplate>
                <asp:TextBox ID="txtRESP" Text='<%# Eval("resp") %>' runat="server" />
            </EditItemTemplate>
            <FooterTemplate>
                <asp:TextBox ID="txtRESPFooter"  runat="server" />
            </FooterTemplate>
        </asp:TemplateField>

        <asp:TemplateField HeaderText="kPI Details">
            <ItemTemplate>
                <asp:Label Text='<%# Eval("kpi") %>' runat="server" Font-Bold="true" />
            </ItemTemplate>
            <EditItemTemplate>
                <asp:TextBox ID="txtKPI"  Text='<%# Eval("kpi") %>' runat="server" />
            </EditItemTemplate>
            <FooterTemplate>
                <asp:TextBox ID="txtKPIFooter"  runat="server" />
            </FooterTemplate>
        </asp:TemplateField>

       <asp:TemplateField HeaderText="Target">
            <ItemTemplate>
                <asp:Label Text='<%# Eval("target") %>' runat="server" />
            </ItemTemplate>
            <EditItemTemplate>
                <asp:TextBox ID="txtTarget" Text='<%# Eval("target") %>' runat="server" />
            </EditItemTemplate>
            <FooterTemplate>
                <asp:TextBox ID="txtTargetFooter"  runat="server" />
            </FooterTemplate>
        </asp:TemplateField>

         <asp:TemplateField HeaderText="Unit of measure">
            <ItemTemplate>
                <asp:Label Text='<%# Eval("uom") %>' runat="server" Font-Bold="true" />
            </ItemTemplate>
            <EditItemTemplate>
                <asp:TextBox ID="txtUOM" Text='<%# Eval("uom") %>' runat="server" />
            </EditItemTemplate>
            <FooterTemplate>
                <asp:TextBox ID="txtUOMFooter"  runat="server" />
            </FooterTemplate>
        </asp:TemplateField>

          <asp:TemplateField HeaderText="Set Order">
            <ItemTemplate>
                <asp:Label Text='<%# Eval("setorder") %>' runat="server" Font-Bold="true" />
            </ItemTemplate>
            <EditItemTemplate>
                <%--<asp:TextBox ID="txtKpiSetOrder" Text='<%# Eval("setorder") %>' runat="server" />--%>
                 <asp:DropDownList ID="txtKpiSetOrder"  SelectedValue='<%# Eval("setorder") %>' runat="server">
                     <asp:ListItem>1</asp:ListItem>
                     <asp:ListItem>2</asp:ListItem>
                     <asp:ListItem>3</asp:ListItem>
                     <asp:ListItem>4</asp:ListItem>
                     <asp:ListItem>5</asp:ListItem>
                     <asp:ListItem>6</asp:ListItem>
                     <asp:ListItem>7</asp:ListItem>
                     <asp:ListItem>8</asp:ListItem>
                     <asp:ListItem>9</asp:ListItem>
                     <asp:ListItem>10</asp:ListItem>
                     <asp:ListItem>11</asp:ListItem>
                     <asp:ListItem>12</asp:ListItem>
                     <asp:ListItem>13</asp:ListItem>
                     <asp:ListItem>14</asp:ListItem>
                     <asp:ListItem>15</asp:ListItem>
                     <asp:ListItem>16</asp:ListItem>
                     <asp:ListItem>17</asp:ListItem>
                     <asp:ListItem>18</asp:ListItem>
                     <asp:ListItem>19</asp:ListItem>
                     <asp:ListItem>20</asp:ListItem>
                     <asp:ListItem>21</asp:ListItem>
                     <asp:ListItem>22</asp:ListItem>
                     <asp:ListItem>23</asp:ListItem>
                     <asp:ListItem>24</asp:ListItem>
                     <asp:ListItem>25</asp:ListItem>
                     <asp:ListItem>26</asp:ListItem>
                     <asp:ListItem>27</asp:ListItem>
                     <asp:ListItem>28</asp:ListItem>
                     <asp:ListItem>29</asp:ListItem>
                     <asp:ListItem>30</asp:ListItem>
                     <asp:ListItem>31</asp:ListItem>
                     <asp:ListItem>32</asp:ListItem>
                     <asp:ListItem>33</asp:ListItem>
                     <asp:ListItem>35</asp:ListItem>
                     <asp:ListItem>36</asp:ListItem>
                     <asp:ListItem>37</asp:ListItem>
                     <asp:ListItem>38</asp:ListItem>
                     <asp:ListItem>39</asp:ListItem>
                     <asp:ListItem>40</asp:ListItem>
                       </asp:DropDownList>
            </EditItemTemplate>
            <FooterTemplate>
                <%--<asp:TextBox ID="txtKpiSetOrderFooter"  runat="server" />--%>

                 <asp:DropDownList ID="txtKpiSetOrderFooter" runat="server">
                     <asp:ListItem>1</asp:ListItem>
                     <asp:ListItem>2</asp:ListItem>
                     <asp:ListItem>3</asp:ListItem>
                     <asp:ListItem>4</asp:ListItem>
                     <asp:ListItem>5</asp:ListItem>
                     <asp:ListItem>6</asp:ListItem>
                     <asp:ListItem>7</asp:ListItem>
                     <asp:ListItem>8</asp:ListItem>
                     <asp:ListItem>9</asp:ListItem>
                     <asp:ListItem>10</asp:ListItem>
                     <asp:ListItem>11</asp:ListItem>
                     <asp:ListItem>12</asp:ListItem>
                     <asp:ListItem>13</asp:ListItem>
                     <asp:ListItem>14</asp:ListItem>
                     <asp:ListItem>15</asp:ListItem>
                     <asp:ListItem>16</asp:ListItem>
                     <asp:ListItem>17</asp:ListItem>
                     <asp:ListItem>18</asp:ListItem>
                     <asp:ListItem>19</asp:ListItem>
                     <asp:ListItem>20</asp:ListItem>
                     <asp:ListItem>21</asp:ListItem>
                     <asp:ListItem>22</asp:ListItem>
                     <asp:ListItem>23</asp:ListItem>
                     <asp:ListItem>24</asp:ListItem>
                     <asp:ListItem>25</asp:ListItem>
                     <asp:ListItem>26</asp:ListItem>
                     <asp:ListItem>27</asp:ListItem>
                     <asp:ListItem>28</asp:ListItem>
                     <asp:ListItem>29</asp:ListItem>
                     <asp:ListItem>30</asp:ListItem>
                     <asp:ListItem>31</asp:ListItem>
                     <asp:ListItem>32</asp:ListItem>
                     <asp:ListItem>33</asp:ListItem>
                     <asp:ListItem>35</asp:ListItem>
                     <asp:ListItem>36</asp:ListItem>
                     <asp:ListItem>37</asp:ListItem>
                     <asp:ListItem>38</asp:ListItem>
                     <asp:ListItem>39</asp:ListItem>
                     <asp:ListItem>40</asp:ListItem>
                 </asp:DropDownList>
            </FooterTemplate>
        </asp:TemplateField>

        <asp:TemplateField HeaderText="Display">
            <ItemTemplate>
                <asp:Label Text='<%# Eval("Display") %>' runat="server" Font-Bold="true" />
            </ItemTemplate>
            <EditItemTemplate>
                <%--<asp:TextBox ID="txtKpiDisplay" Text='<%# Eval("Display") %>' runat="server" />--%>
                <asp:DropDownList ID="txtKpiDisplay" SelectedValue='<%# Eval("Display").ToString().Trim().ToUpper() %>'  runat="server">
                     <asp:ListItem>Y</asp:ListItem>
                     <asp:ListItem>N</asp:ListItem>
                      </asp:DropDownList>
            </EditItemTemplate>
            <FooterTemplate>
                <%--<asp:TextBox ID="txtKpiDisplayFooter"  runat="server" />--%>
                <asp:DropDownList ID="txtKpiDisplayFooter" runat="server">
                     <asp:ListItem>Y</asp:ListItem>
                     <asp:ListItem>N</asp:ListItem>
                     
                      </asp:DropDownList>
            </FooterTemplate>
        </asp:TemplateField>

         <asp:TemplateField HeaderText="Calculate by Month/Year/Total" Visible="false">
            <ItemTemplate>
                <asp:Label Text='<%# Eval("month_or_year") %>' runat="server" Font-Bold="true" />
            </ItemTemplate>
            <EditItemTemplate>
                <%--<asp:TextBox ID="txtMonthOrYear" Text='<%# Eval("month_or_year") %>' runat="server" />--%>
                 <asp:DropDownList ID="txtMonthOrYear" SelectedValue='<%# Eval("month_or_year").ToString().Trim().ToUpper() %>'  runat="server">
                     <asp:ListItem>Y</asp:ListItem>
                     <asp:ListItem>M</asp:ListItem>
                      <asp:ListItem>T</asp:ListItem>
                      </asp:DropDownList>
            </EditItemTemplate>
            <FooterTemplate>
                <%--<asp:TextBox ID="txtMonthOrYearFooter"  runat="server" />--%>
                 <asp:DropDownList ID="txtMonthOrYearFooter" runat="server">
                     <asp:ListItem>Y</asp:ListItem>
                     <asp:ListItem>M</asp:ListItem>
                     <asp:ListItem>T</asp:ListItem>
                      </asp:DropDownList>
            </FooterTemplate>
        </asp:TemplateField>

          <asp:TemplateField HeaderText="Upper Limit Colour">
            <ItemTemplate>
                <asp:Label Text='<%# Eval("upper_limit_colour") %>' runat="server" Font-Bold="true" />
            </ItemTemplate>
            <EditItemTemplate>
                <%--<asp:TextBox ID="txtDisplay" Text='<%# Eval("Display") %>' runat="server" />--%>
                  <asp:DropDownList ID="txtUpperLimitColour" SelectedValue='<%# Eval("upper_limit_colour").ToString().Trim().ToUpper() %>'   runat="server">
                     <asp:ListItem>RED</asp:ListItem>
                     <asp:ListItem>GREEN</asp:ListItem>
                      </asp:DropDownList>
            </EditItemTemplate>
            <FooterTemplate>
                <%--<asp:TextBox ID="txtDisplayFooter"  runat="server" />--%>
                 <asp:DropDownList ID="txtUpperLimitColourFooter" runat="server">
                     <asp:ListItem>RED</asp:ListItem>
                     <asp:ListItem>GREEN</asp:ListItem>
                      </asp:DropDownList>
            </FooterTemplate>
        </asp:TemplateField>

         <asp:TemplateField HeaderText="Lower Limit Colour">
            <ItemTemplate>
                <asp:Label Text='<%# Eval("lower_limit_colour") %>' runat="server" Font-Bold="true" />
            </ItemTemplate>
            <EditItemTemplate>
                <%--<asp:TextBox ID="txtDisplay" Text='<%# Eval("Display") %>' runat="server" />--%>
                  <asp:DropDownList ID="txtLowerLimitColour" SelectedValue='<%# Eval("lower_limit_colour").ToString().Trim().ToUpper() %>'   runat="server">
                     <asp:ListItem>RED</asp:ListItem>
                     <asp:ListItem>GREEN</asp:ListItem>
                      </asp:DropDownList>
            </EditItemTemplate>
            <FooterTemplate>
                <%--<asp:TextBox ID="txtDisplayFooter"  runat="server" />--%>
                 <asp:DropDownList ID="txtLowerLimitColourFooter" runat="server">
                     <asp:ListItem>RED</asp:ListItem>
                     <asp:ListItem>GREEN</asp:ListItem>
                      </asp:DropDownList>
            </FooterTemplate>
        </asp:TemplateField>

        <asp:TemplateField>
            <ItemTemplate>
             <%--<asp:ImageButton ImageUrl=" " runat="server" CommandName="Edit" ToolTip ="Edit"           
            --%>
                <asp:LinkButton runat="server"  CommandName="Edit"  CssClass="glyphicon glyphicon-pencil" ToolTip ="Edit"></asp:LinkButton>
                <%--<asp:LinkButton runat="server"  CommandName="delete" ToolTip ="Edit">delete</asp:LinkButton>--%>
            </ItemTemplate>
            <EditItemTemplate>
                 <asp:LinkButton runat="server"  CommandName="Update" CssClass="glyphicon glyphicon-floppy-disk" ToolTip ="Update entry" ></asp:LinkButton>
                <asp:LinkButton runat="server"  CommandName="Cancel" CssClass="glyphicon glyphicon-remove" ToolTip ="Cancel Edit Mode"></asp:LinkButton>
            </EditItemTemplate>
            <FooterTemplate>
                 <asp:LinkButton runat="server"  CommandName="AddNew"  CssClass="glyphicon glyphicon-plus" ToolTip ="Add New" ></asp:LinkButton>
               
            </FooterTemplate>

        </asp:TemplateField>

         


    </Columns>

        </asp:GridView>
       <%-- </asp:Panel>  --%>     
    <br />
    <asp:Label ID="lblKpiSuccess" runat="server" Text="" ForeColor="Green"></asp:Label><br />
    <asp:Label ID="lblKpiError" runat="server" Text="" ForeColor="Red"></asp:Label>
               
                </td>      
           
            </tr>
        </table>
     
</asp:Content>
