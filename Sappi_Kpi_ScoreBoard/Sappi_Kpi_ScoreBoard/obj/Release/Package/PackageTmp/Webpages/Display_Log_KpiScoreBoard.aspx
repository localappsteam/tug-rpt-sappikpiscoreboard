﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Display_Log_KpiScoreBoard.aspx.cs" Inherits="Sappi_Kpi_ScoreBoard.Webpages.Display_Log_KpiScoreBoard" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <link href="../Content/IconSizes.css" rel="stylesheet" />
    <link href="../Content/GridviewTable.css" rel="stylesheet" />
    <link href="../Content/GridViewReport.css" rel="stylesheet" />
    <link href="../Content/GridviewScroll.css" rel="stylesheet" />

    <asp:Panel ID="Panel1" ScrollBars="Auto" runat="server">
        <asp:GridView ID="gv_Log" runat="server" CssClass="mytable">
         
        </asp:GridView>
    </asp:Panel>

</asp:Content>
