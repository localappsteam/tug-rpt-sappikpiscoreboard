﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Sappi_Kpi_ScoreBoard
{
    public partial class Display_KpiScoreBoard : System.Web.UI.Page
    {
        Classes.DBBackend DBBackend = new Classes.DBBackend();
        Classes.Checks checks = new Classes.Checks();
        DataTable dt = new DataTable();
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                try
                {
                    populateGV_Display_Dashboard();
                    ChangeColumnHeadersToIncludeYear();
                }
                catch (Exception)
                {

                    gvDisplay_ScoreBoard.Visible = false;
                    lblNoData.Visible = true;
                }
                
            }
        }

        private void ChangeColumnHeadersToIncludeYear()
        {
            gvDisplay_ScoreBoard.HeaderRow.Cells[7].Text = "October " + (Convert.ToInt32(dt.Rows[0]["Group_year"]) - 1).ToString();
            gvDisplay_ScoreBoard.HeaderRow.Cells[8].Text = "November " + (Convert.ToInt32(dt.Rows[0]["Group_year"]) - 1).ToString();
            gvDisplay_ScoreBoard.HeaderRow.Cells[9].Text = "December " + (Convert.ToInt32(dt.Rows[0]["Group_year"]) - 1).ToString();
            gvDisplay_ScoreBoard.HeaderRow.Cells[10].Text = "January " + Convert.ToInt32(dt.Rows[0]["Group_year"]).ToString();
            gvDisplay_ScoreBoard.HeaderRow.Cells[11].Text = "February " + Convert.ToInt32(dt.Rows[0]["Group_year"]).ToString();
            gvDisplay_ScoreBoard.HeaderRow.Cells[12].Text = "March " + Convert.ToInt32(dt.Rows[0]["Group_year"]).ToString();
            gvDisplay_ScoreBoard.HeaderRow.Cells[13].Text = "April " + Convert.ToInt32(dt.Rows[0]["Group_year"]).ToString();
            gvDisplay_ScoreBoard.HeaderRow.Cells[14].Text = "May " + Convert.ToInt32(dt.Rows[0]["Group_year"]).ToString();
            gvDisplay_ScoreBoard.HeaderRow.Cells[15].Text = "June " + Convert.ToInt32(dt.Rows[0]["Group_year"]).ToString();
            gvDisplay_ScoreBoard.HeaderRow.Cells[16].Text = "July " + Convert.ToInt32(dt.Rows[0]["Group_year"]).ToString();
            gvDisplay_ScoreBoard.HeaderRow.Cells[17].Text = "August " + Convert.ToInt32(dt.Rows[0]["Group_year"]).ToString();
            gvDisplay_ScoreBoard.HeaderRow.Cells[18].Text = "September " + (Convert.ToInt32(dt.Rows[0]["Group_year"])).ToString();
            gvDisplay_ScoreBoard.HeaderRow.Cells[19].Text = "Year To Date";
        }

        private void populateGV_Display_Dashboard()
        {
            
            DataRow dr = null;
            dt = DBBackend.gv_Display_KpiScoreBoard();

            if (dt.Rows.Count > 0)
            {
                gvDisplay_ScoreBoard.DataSource = dt;
                gvDisplay_ScoreBoard.DataBind();
               
                // lblGroupNameDetails.Text = dt.Rows[0]["Group_Name"].ToString();
            }
            else
            {
                dr = dt.NewRow();
                dr["Kpi_id"] = 0;
                dr["Group_id"] = 1;
                dr["Group_name"] = "";
               // dr["setorder"] = 0;
               // dr["display"] = "Y";

                dt.Rows.Add(dr);
                gvDisplay_ScoreBoard.DataSource = dt;
                gvDisplay_ScoreBoard.DataBind();
                gvDisplay_ScoreBoard.Rows[0].Cells.Clear();
                gvDisplay_ScoreBoard.Rows[0].Cells.Add(new TableCell());
                gvDisplay_ScoreBoard.Rows[0].Cells[0].ColumnSpan = dt.Columns.Count;
                gvDisplay_ScoreBoard.Rows[0].Cells[0].Text = "No Data Found..!";
                gvDisplay_ScoreBoard.Rows[0].Cells[0].HorizontalAlign = HorizontalAlign.Center;
               // lblGroupNameDetails.Text = "";
            }
           
        }

        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void gvDisplay_ScoreBoard_RowDataBound1(object sender, GridViewRowEventArgs e)
        {
            bool check;
           // try
            {
                switch (checks.checkAccessLevel())
                {
                    case 0:
                        check = false;
                        break;

                    case 1:
                        check = true;
                        break;

                    case 2:
                        check = true;
                        break;

                    default:
                        check = false;
                        break;
                }

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label _lblGroupName = (Label)e.Row.FindControl("lblGroupName");
                    Label _lblTarget = (Label)e.Row.FindControl("lblTarget");
                    //Label _lblOctober = (Label)e.Row.FindControl("lblOctober");
                    LinkButton _lnkEdit = (LinkButton)e.Row.FindControl("lnkEdit");
                    LinkButton _lnkUpdate = (LinkButton)e.Row.FindControl("lnkUpdate");
                    LinkButton _lnkCancel = (LinkButton)e.Row.FindControl("lnkCancel");
                    if (e.Row.RowType == DataControlRowType.DataRow)
                    {
                      
                        foreach (TableCell cell in e.Row.Cells)
                        {
                          //  LnkBntControlState(_lnkEdit, check);
                          //  LnkBntControlState(_lnkUpdate, check);
                         //   LnkBntControlState(_lnkCancel, check);
                            if (check == false)
                            {
                             //   gvDisplay_ScoreBoard.Columns[0].Visible = check;
                            }

                          //  if (_lblGroupName.Text !="" )
                          //  {
                          //   cell.BackColor = Color.LightGray;
                          //   _lnkEdit.Visible = false;
                          //  }
                           
                        }
                    }
                }
               // ChangeColumnHeadersToIncludeYear();
               // UpdatePanel1.Update();
            }
           // catch (Exception)
            {

          //      throw;
            }
          
        }

        protected void LnkBntControlState(LinkButton lnkbnt,bool check)
        {
            lnkbnt.Visible = check;
            lnkbnt.Enabled = check;
        }
        protected void gvDisplay_ScoreBoard_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gvDisplay_ScoreBoard.EditIndex = e.NewEditIndex;
            populateGV_Display_Dashboard();
          //  ChangeColumnHeadersToIncludeYear();
        }

        protected void gvDisplay_ScoreBoard_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gvDisplay_ScoreBoard.EditIndex = -1;
            populateGV_Display_Dashboard();
          // ChangeColumnHeadersToIncludeYear();
        }

        protected void gvDisplay_ScoreBoard_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                
                var kpi_id = gvDisplay_ScoreBoard.DataKeys[e.RowIndex].Value.ToString();
                var txtJanuary = (gvDisplay_ScoreBoard.Rows[e.RowIndex].FindControl("txtJanuary") as TextBox).Text.Trim();
                var txtFebruary = (gvDisplay_ScoreBoard.Rows[e.RowIndex].FindControl("txtFebruary") as TextBox).Text.Trim();
                var txtMarch = (gvDisplay_ScoreBoard.Rows[e.RowIndex].FindControl("txtMarch") as TextBox).Text.Trim();
                var txtApril = (gvDisplay_ScoreBoard.Rows[e.RowIndex].FindControl("txtApril") as TextBox).Text.Trim();
                var txtMay = (gvDisplay_ScoreBoard.Rows[e.RowIndex].FindControl("txtMay") as TextBox).Text.Trim();
                var txtJune = (gvDisplay_ScoreBoard.Rows[e.RowIndex].FindControl("txtJune") as TextBox).Text.Trim();
                var txtJuly = (gvDisplay_ScoreBoard.Rows[e.RowIndex].FindControl("txtJuly") as TextBox).Text.Trim();
                var txtAugust = (gvDisplay_ScoreBoard.Rows[e.RowIndex].FindControl("txtAugust") as TextBox).Text.Trim();
                var txtSeptember = (gvDisplay_ScoreBoard.Rows[e.RowIndex].FindControl("txtSeptember") as TextBox).Text.Trim();
                var txtOctober = (gvDisplay_ScoreBoard.Rows[e.RowIndex].FindControl("txtOctober") as TextBox).Text.Trim();
                var txtNovember = (gvDisplay_ScoreBoard.Rows[e.RowIndex].FindControl("txtNovember") as TextBox).Text.Trim();
                var txtDecember = (gvDisplay_ScoreBoard.Rows[e.RowIndex].FindControl("txtDecember") as TextBox).Text.Trim();
                var txtYTD = (gvDisplay_ScoreBoard.Rows[e.RowIndex].FindControl("txtYTD") as TextBox).Text.Trim();
                DBBackend.Update_KPI_Results(Convert.ToInt64(kpi_id),
                                             checks.checkIfNull(txtJanuary),
                                             checks.checkIfNull(txtFebruary),
                                             checks.checkIfNull(txtMarch),
                                             checks.checkIfNull(txtApril),
                                             checks.checkIfNull(txtMay),
                                             checks.checkIfNull(txtJune),
                                             checks.checkIfNull(txtJuly),
                                             checks.checkIfNull(txtAugust),
                                             checks.checkIfNull(txtSeptember),
                                             checks.checkIfNull(txtOctober),
                                             checks.checkIfNull(txtNovember),
                                             checks.checkIfNull(txtDecember),
                                             checks.checkIfNull(txtYTD));
                

                //  lblKpiSuccess.Text = "Data Added";
                //  lblKpiError.Text = "";
                gvDisplay_ScoreBoard.EditIndex = -1;
                populateGV_Display_Dashboard();
              //  ChangeColumnHeadersToIncludeYear();
            }
            catch (Exception ex)
            {
                throw;
                // lblKpiSuccess.Text = "";
               // lblKpiError.Text = ex.Message;
            }
        }
        protected Color GetStatusString1(string target, string month, string upperlimitcolour, string lowerlimitcolour )
        {
          
            return checks.CheckStatusString(target,month,upperlimitcolour,lowerlimitcolour);
        }

        protected bool ActiveMonth(int monthnumber)
        {
            bool check;
            if (checks.CheckForAccess()==true)
           {
                check = checks.CheckActiveMonth(monthnumber);
           }
        else
            {
                check = true;
            }
         return check;
        }

        protected void gvDisplay_ScoreBoard_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void gvDisplay_ScoreBoard_RowCommand(object sender, GridViewCommandEventArgs e)
        {

        }
    }
}
