﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="UserAccessAllocation_CRUD_KpiScoreBoard.aspx.cs" Inherits="Sappi_Kpi_ScoreBoard.Webpages.UserAccessAllocation_CRUD_KpiScoreBoard" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

      <script src="../Scripts/jquery-2.2.3.min.js"></script>
    <script src="../Scripts/jquery-ui-1.12.1.js"></script>
    <link href="../Content/themes/base/jquery-ui.min.css" rel="stylesheet" />

    <link href="../Content/IconSizes.css" rel="stylesheet" />
    <link href="../Content/GridviewTable.css" rel="stylesheet" />   
    <link href="../Content/GridViewReport.css" rel="stylesheet" />
    <link href="../Content/GridviewScroll.css" rel="stylesheet" />
    <script src="../Scripts/GridviewScroll.js"></script> 

    <table>
        <tr>
            <td>
    <h1> User rights allocation </h1>
                </td>
       </tr>
      <%-- <tr>
            <td>
                <asp:Label ID="lblUsername" runat ="server" Text="UserName"></asp:Label>
                <asp:TextBox ID="txtUserName" runat="server"></asp:TextBox>
                <asp:Button ID="bntUserName" runat="server" Text="Button" OnClick="bntUserName_Click" />
                
                <asp:Label ID="lblmessage" runat ="server"></asp:Label>
            </td>
        </tr>--%>
        <tr>
            <td>
                
                <asp:GridView ID="gv_UserAccess" runat="server"  AutoGenerateColumns="False"     
                ShowFooter="true" DataKeyNames="User_ID" OnRowCommand="gv_UserAccess_RowCommand"  OnRowEditing="gv_UserAccess_RowEditing"
                OnRowCancelingEdit="gv_UserAccess_RowCancelingEdit" OnRowUpdating="gv_UserAccess_RowUpdating" ShowHeaderWhenEmpty="true" 
                OnRowDataBound="gv_UserAccess_RowDataBound" OnRowDeleting="gv_UserAccess_RowDeleting" Width="1260px">
 
                    <Columns>
       
         <asp:TemplateField HeaderText="Date Created">
            <ItemTemplate>
                <asp:Label Text='<%# Eval("Date_Created") %>' runat="server"  Font-Bold="true" />
            </ItemTemplate>
            <EditItemTemplate>
                <asp:TextBox ID="txtDateCreated"  Text='<%# Eval("Date_Created") %>' Enabled="false" runat="server" />
               
            </EditItemTemplate>
            <FooterTemplate>
                <asp:TextBox ID="txtDateCreatedFooter"  runat="server" Enabled="false" />               
            </FooterTemplate>
        </asp:TemplateField>

        <asp:TemplateField HeaderText="UserName">
            <ItemTemplate>
                <asp:Label Text='<%# Eval("User_name") %>' runat="server" Font-Bold="true" />
            </ItemTemplate>
            <EditItemTemplate>
                <asp:TextBox ID="txtUserName" Text='<%# Eval("User_name") %>' runat="server" />
                <asp:LinkButton ID="lnkCheckUserName" runat="server" cssClass="btn btn-primary btn-xs glyphicon glyphicon-search" ToolTip="Check UserName" OnClick="lnkCheckUserName_Click"></asp:LinkButton>
            </EditItemTemplate>
            <FooterTemplate>
                <asp:TextBox ID="txtUserNameFooter"  runat="server"/>
               
                <asp:LinkButton ID="lnkCheckUserNameFooter_Click" runat="server" cssClass="btn btn-primary btn-xs glyphicon glyphicon-search" ToolTip="Check UserName" OnClick="lnkCheckUserNameFooter_Click"></asp:LinkButton>
        </FooterTemplate>
        </asp:TemplateField>

        <asp:TemplateField HeaderText="Access Level">
            <ItemTemplate>
                <asp:Label Text='<%# Eval("Access_Level") %>' runat="server" Font-Bold="true" />
            </ItemTemplate>
            <EditItemTemplate>
                <%--<asp:TextBox ID="txtDisplay" Text='<%# Eval("Display") %>' runat="server" />--%>
                  <asp:DropDownList ID="txtAccessLevel" SelectedValue='<%# Eval("Access_Level").ToString().Trim().ToUpper() %>'   runat="server">
                     <asp:ListItem>ADMIN</asp:ListItem>
                     <asp:ListItem>EDIT</asp:ListItem>
                      </asp:DropDownList>
            </EditItemTemplate>
            <FooterTemplate>
                <%--<asp:TextBox ID="txtDisplayFooter"  runat="server" />--%>
                 <asp:DropDownList ID="txtAccessLevelFooter" runat="server">
                     <asp:ListItem>ADMIN</asp:ListItem>
                     <asp:ListItem>EDIT</asp:ListItem>
                      </asp:DropDownList>
            </FooterTemplate>
        </asp:TemplateField>
     
        <asp:TemplateField>
            <ItemTemplate>
            
                <asp:LinkButton runat="server"  CommandName="Edit" CssClass="glyphicon glyphicon-pencil" ToolTip ="Edit Entry"></asp:LinkButton>
                <asp:LinkButton runat="server"  CommandName="Delete" CssClass="glyphicon glyphicon-trash" ToolTip ="Delete Entry"></asp:LinkButton>
            </ItemTemplate>
            <EditItemTemplate>
                 <asp:LinkButton runat="server"  CommandName="Update" CssClass="glyphicon glyphicon-floppy-disk" ToolTip ="Update entry" ></asp:LinkButton>
                <asp:LinkButton runat="server"  CommandName="Cancel" CssClass="glyphicon glyphicon-remove" ToolTip ="Cancel Edit Mode"></asp:LinkButton>
            </EditItemTemplate>
            <FooterTemplate>
                 <asp:LinkButton runat="server"  CommandName="AddNew" CssClass="glyphicon glyphicon-plus" ToolTip ="Add New" ></asp:LinkButton>
            </FooterTemplate>

        </asp:TemplateField>

    </Columns>


                </asp:GridView>

            </td>
        </tr>

        </table>

<asp:Label ID="lblMessageSuccess" runat="server" ForeColor="Green"></asp:Label>
<asp:Label ID="lblMessageError" runat="server" ForeColor="Red"></asp:Label>
</asp:Content>
