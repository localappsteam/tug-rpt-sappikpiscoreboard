﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Sappi_Kpi_ScoreBoard.Webpages
{
    public partial class Display_Log_KpiScoreBoard : System.Web.UI.Page
    {
        Classes.DBBackend DBBackend = new Classes.DBBackend();
        protected void Page_Load(object sender, EventArgs e)
        {
        if(!IsPostBack)
            {
                gv_Log.DataSource = DBBackend.gv_DisplayLog();
                gv_Log.DataBind();
            }
        }

        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}