﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Sappi_Kpi_ScoreBoard.Webpages
{
    public partial class UserAccessAllocation_CRUD_KpiScoreBoard : System.Web.UI.Page
    {
        Classes.DBBackend DBBackend = new Classes.DBBackend();
        Classes.Checks Checks = new Classes.Checks();
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                redirect();
                populateGV_UserAccess();
            }

        }

        private void populateGV_UserAccess()
        {
            var dt = new DataTable();
            DataRow dr = null;
            dt = DBBackend.gv_DisplayUserAccess();

            if (dt.Rows.Count > 0)
            {
                gv_UserAccess.DataSource = dt;
                gv_UserAccess.DataBind();
             //  txtGroupID.Text = dt.Rows[0]["Group_ID"].ToString();
           //    lblGroupNameDetails.Text = dt.Rows[0]["Group_Name"].ToString();
            }
            else
            {
                dr = dt.NewRow();

                dr["User_Name"] = "";
                dr["User_ID"] = 0;
                dr["Date_Created"] =DateTime.Now;
                dr["Access_Level"] = "Edit";
                

                dt.Rows.Add(dr);
                gv_UserAccess.DataSource = dt;
                gv_UserAccess.DataBind();
                gv_UserAccess.Rows[0].Cells.Clear();
                gv_UserAccess.Rows[0].Cells.Add(new TableCell());
                gv_UserAccess.Rows[0].Cells[0].ColumnSpan = dt.Columns.Count;
                gv_UserAccess.Rows[0].Cells[0].Text = "No Data Found..!";
                gv_UserAccess.Rows[0].Cells[0].HorizontalAlign = HorizontalAlign.Center;
            }
        }
     
        protected void gv_UserAccess_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("AddNew"))
            {
                try
                {

                    var txtUserNameFooter = (gv_UserAccess.FooterRow.FindControl("txtUserNameFooter") as TextBox).Text.Trim();
                    var txtAccessLevelFooter = (gv_UserAccess.FooterRow.FindControl("txtAccessLevelFooter") as DropDownList).Text.Trim();

                    DBBackend.Create_User_Access(txtUserNameFooter, txtAccessLevelFooter);

                    lblMessageSuccess.Text = "Data Added";
                    lblMessageError.Text = "";
                    populateGV_UserAccess();
                }
                catch (Exception ex)
                {
                    lblMessageSuccess.Text = "";
                    lblMessageError.Text = ex.Message;
                }

            }
        }

        protected void gv_UserAccess_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gv_UserAccess.EditIndex = e.NewEditIndex;
            populateGV_UserAccess();
        }

        protected void gv_UserAccess_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gv_UserAccess.EditIndex = -1;
            populateGV_UserAccess();
        }

        protected void gv_UserAccess_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                var txtUserID = gv_UserAccess.DataKeys[e.RowIndex].Value.ToString();
                var txtUserName = (gv_UserAccess.Rows[e.RowIndex].FindControl("txtUserName") as TextBox).Text.Trim();
                var txtAccessLevel = (gv_UserAccess.Rows[e.RowIndex].FindControl("txtAccessLevel") as DropDownList).Text.Trim();

                DBBackend.Update_User_Access(Convert.ToInt64(txtUserID), 
                                             txtUserName, 
                                             txtAccessLevel);


                lblMessageSuccess.Text = "Selected Row Updated";
                lblMessageError.Text = "";
                gv_UserAccess.EditIndex = -1;
                populateGV_UserAccess();


            }
            catch (Exception ex)
            {
                lblMessageSuccess.Text = "";
                lblMessageError.Text = ex.Message;
            }
        }

        protected void gv_UserAccess_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                var txtUserID = gv_UserAccess.DataKeys[e.RowIndex].Value.ToString();

                DBBackend.Delete_User_Access(Convert.ToInt64(txtUserID));

                lblMessageSuccess.Text = "Selected Row Has been Deleted";
                lblMessageError.Text = "";
                gv_UserAccess.EditIndex = -1;
                populateGV_UserAccess();


            }
            catch (Exception ex)
            {
                lblMessageSuccess.Text = "";
                lblMessageError.Text = ex.Message;
            }
       
    }
        protected void lnkCheckUserNameFooter_Click(object sender, EventArgs e)
        {
           //string id = ((LinkButton)sender).CommandArgument.ToString();
            TextBox txtUserNameFooter = (TextBox)gv_UserAccess.FooterRow.FindControl("txtUserNameFooter");
            ShowAlertMessage(txtUserNameFooter);
        }

        protected void lnkCheckUserName_Click(object sender, EventArgs e)
        {

            GridViewRow gridViewRow = (GridViewRow)(sender as Control).Parent.Parent;
            int index = gridViewRow.RowIndex;

            TextBox txtUserName = (TextBox)gv_UserAccess.Rows[index].FindControl("txtUserName");
            ShowAlertMessage(txtUserName);
        }

        private void ShowAlertMessage(TextBox txtUserName)
        {
            if (DBBackend.FindADUserDetails(txtUserName.Text) == true)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('✅ ["+ txtUserName.Text.ToUpper().Trim() + "] Username is Correct');", true);
            }
            else
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('⚠️ Unable to Find Username [" + txtUserName.Text.ToUpper().Trim() + "]: Check Username and try again');", true);
        }

        protected void gv_UserAccess_RowDataBound(object sender, GridViewRowEventArgs e)
        {
           
        }

        protected void bntUserName_Click(object sender, EventArgs e)
        {
            
           // txtUserName.Text= DBBackend.Get_User_Rights(Checks.CheckCurrentIdentity()).ToString();
        }

       protected void redirect()
       {
          if(Checks.CheckForAccess()==true)
                    Response.Redirect("DisplayDashBoard_KpiScoreBoard.aspx");
       }
           
    }
}