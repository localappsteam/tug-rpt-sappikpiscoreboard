﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" EnableEventValidation = "false" CodeBehind="DisplayDashBoard_KpiScoreBoard.aspx.cs" Inherits="Sappi_Kpi_ScoreBoard.Webpages.DisplayDashBoard_KpiScoreBoard1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
       <script src="../Scripts/jquery-2.2.3.min.js"></script>
    <script src="../Scripts/jquery-ui-1.12.1.js"></script>
    <link href="../Content/themes/base/jquery-ui.min.css" rel="stylesheet" />
   
   
    <link href="../Content/GridviewTable.css" rel="stylesheet" />
    <link href="../Content/GridViewReport.css" rel="stylesheet" />
    <link href="../Content/GridviewScroll.css" rel="stylesheet" />
    <script src="../Scripts/GridviewScroll.js"></script>
    <link href="../Content/IconSizes.css" rel="stylesheet" />
   
  
<style id="gridStyles" runat="server" type="text/css">
    body
    {
        font-family: Arial;
        font-size: 10pt;
    }
    table
    {
        border: 1px solid #ccc;
        border-collapse: collapse;
    }
    table th
    {
        color: #0090CB;
        font-weight: bold;
    }
    table th, table td
    {
        padding: 5px;
        border: 1px solid #ccc;
    }
    table, table table td
    {
        border: 0px solid #ccc;
    }
    thead
    {
        display: table-header-group;
    }
    tfoot
    {
        display: table-footer-group;
    }
</style>

    
<script type="text/javascript">
    function PrintGrid(html, css) {
      //  var newStr = html.replace(/[^a-zA-Z0-9-]/g, "");
        var printWin = window.open('', '', 'left=0,top=0,width=800,height=800,scrollbars=1');
        printWin.document.write('<style type = "text/css">' + css + '</style>');
        printWin.document.write(html);
        printWin.document.close();
        printWin.focus();
        printWin.print();
        printWin.close();
    };
</script>



    <asp:Button ID="btnPrintAll" runat="server" Text="Print" OnClick="PrintGridView_Click" CommandArgument="All"/>
    <asp:Button ID="btnXls" runat="server" Text="Export To Excel" OnClick="btnXls_Click" CommandArgument="All"/>
    <asp:Button ID="bntRefresh" runat="server" Text="Refresh" OnClick="bntRefresh_Click" CommandArgument="All"/>

    <asp:UpdatePanel ID="UpdatePanel1" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">   
     <ContentTemplate>
        

       <%--  <asp:Panel ID="Panel1" ScrollBars="Both"  runat="server" ClientIDMode="Static">
           <div style="height:calc(100vh-240px);">
               <asp:PlaceHolder ID="phPrince" runat="server">--%>
    <asp:GridView id="gvDisplay_ScoreBoard" Font-Bold="true" runat="server" CssClass="mytable"  AutoGenerateColumns="False" OnRowDataBound="gvDisplay_ScoreBoard_RowDataBound" ShowHeaderWhenEmpty="true"
        DataKeyNames="Kpi_id"  OnRowEditing="gvDisplay_ScoreBoard_RowEditing" OnRowCancelingEdit="gvDisplay_ScoreBoard_RowCancelingEdit"
        OnRowUpdating="gvDisplay_ScoreBoard_RowUpdating" >
       
        <Columns>
            <asp:TemplateField>
                <ItemTemplate>

                    <asp:LinkButton ID="lnkEdit" runat="server" CommandName="Edit" CssClass="glyphicon glyphicon-pencil" ToolTip="Edit"></asp:LinkButton>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:LinkButton ID="lnkUpdate" runat="server" CommandName="Update" CssClass="glyphicon glyphicon-floppy-disk" ToolTip="Update entry"></asp:LinkButton>
                    <asp:LinkButton ID="lnkCancel" runat="server" CommandName="Cancel" CssClass="glyphicon glyphicon-remove" ToolTip="Cancel Edit Mode"></asp:LinkButton>
                </EditItemTemplate>
                <FooterTemplate>
                    <%--<asp:LinkButton runat="server"  CommandName="AddNew" ToolTip ="Edit" >AddNew</asp:LinkButton>--%>
                </FooterTemplate>

            </asp:TemplateField>
            <asp:TemplateField HeaderText="Title">
                <ItemTemplate>
                    <asp:Label ID="lblGroupName" Text='<%# Eval("GROUP_NAME") %>' runat="server" />
                </ItemTemplate>
                <ItemStyle Font-Bold="true" Font-Italic="false" Font-Size="12px" />
                <EditItemTemplate>
                    <asp:TextBox ID="txtGroupName" Text='<%# Eval("GROUP_NAME") %>' runat="server" Enabled="false" />
                </EditItemTemplate>

                <FooterTemplate>
                    <asp:TextBox ID="txtGroupNameFooter" runat="server" />
                </FooterTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Objective" ItemStyle-Wrap="true">
                <ItemTemplate>
                    <asp:Label Text='<%# Eval("OBJECTIVE") %>' runat="server" />
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="txtObjective" Text='<%# Eval("OBJECTIVE") %>' runat="server" Enabled="false" />
                </EditItemTemplate>
                <FooterTemplate>
                    <asp:TextBox ID="txtObjectiveFooter" runat="server" />
                </FooterTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="RESP">
                <ItemTemplate>
                    <asp:Label Text='<%# Eval("RESP") %>' runat="server" />
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="txtRESP" Text='<%# Eval("RESP") %>' runat="server" Enabled="false" />
                </EditItemTemplate>
                <FooterTemplate>
                    <asp:TextBox ID="txtRESPFooter" runat="server" />
                </FooterTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="KPI">
                <ItemTemplate>
                    <asp:Label Text='<%# Eval("KPI") %>' runat="server" />
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="txtKPI" Text='<%# Eval("KPI") %>' runat="server" Enabled="false" />
                </EditItemTemplate>
                <FooterTemplate>
                    <asp:TextBox ID="txtKPIFooter" runat="server" />
                </FooterTemplate>
            </asp:TemplateField>

             <asp:TemplateField HeaderText="Target">
                <ItemTemplate>
                    <asp:Label ID="lblTarget" Text='<%# Eval("TARGET") %>' runat="server" />
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="txtTarget" Text='<%# Eval("TARGET") %>' runat="server" Enabled="false" />
                </EditItemTemplate>
                <FooterTemplate>
                    <asp:TextBox ID="txtTargetFooter" runat="server" />
                </FooterTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="UOM">
                <ItemTemplate>
                    <asp:Label Text='<%# Eval("UOM") %>' runat="server" />
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="txtUOM" Text='<%# Eval("UOM") %>' runat="server" Enabled="false" />
                </EditItemTemplate>
                <FooterTemplate>
                    <asp:TextBox ID="txtUOMFooter" runat="server" />
                </FooterTemplate>
            </asp:TemplateField>

            <asp:TemplateField>
                <HeaderTemplate>
                    October
                </HeaderTemplate>
                <%--<ItemStyle BackColor='<%# GetStatusString1(Eval("Target").ToString(),Eval("OCTOBER").ToString())%>' />--%>
                <ItemTemplate>

                    <asp:Label ID="lblOctober" Text='<%#Eval("OCTOBER")%>' ForeColor='<%# GetStatusString1(Eval("Target").ToString(),Eval("OCTOBER").ToString(),Eval("UPPER_LIMIT_COLOUR").ToString(),Eval("LOWER_LIMIT_COLOUR").ToString())%>' runat="server" />
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="txtOctober" Text='<%#Eval("OCTOBER")%>' Enabled='<%# ActiveMonth(10)%>' runat="server" />
                    <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtOctober" ForeColor="Red" ErrorMessage="Entry Not Valid" ValidationExpression="^\s*(?:\d+|\d*\.\d+)?\s*$"></asp:RegularExpressionValidator>--%>
                </EditItemTemplate>
                <FooterTemplate>
                    <asp:TextBox ID="txtOctoberFooter" runat="server" />
                </FooterTemplate>
            </asp:TemplateField>

            <asp:TemplateField>
                <HeaderTemplate>
                    November
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Label Text='<%# Eval("NOVEMBER") %>' ForeColor='<%# GetStatusString1(Eval("Target").ToString(),Eval("NOVEMBER").ToString(),Eval("UPPER_LIMIT_COLOUR").ToString(),Eval("LOWER_LIMIT_COLOUR").ToString())%>' runat="server" />
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="txtNovember" Text='<%# Eval("NOVEMBER") %>' Enabled='<%# ActiveMonth(11)%>' runat="server" />
                </EditItemTemplate>
                <FooterTemplate>
                    <asp:TextBox ID="txtNovemberFooter" runat="server" />
                </FooterTemplate>
            </asp:TemplateField>

            <asp:TemplateField>
                <HeaderTemplate>
                    December
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Label Text='<%# Eval("DECEMBER") %>' ForeColor='<%# GetStatusString1(Eval("Target").ToString(),Eval("DECEMBER").ToString(),Eval("UPPER_LIMIT_COLOUR").ToString(),Eval("LOWER_LIMIT_COLOUR").ToString())%>' runat="server" />
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="txtDecember" Text='<%# Eval("DECEMBER") %>' Enabled='<%# ActiveMonth(12)%>' runat="server" />
                </EditItemTemplate>
                <FooterTemplate>
                    <asp:TextBox ID="txtDecemberFooter" runat="server" />
                </FooterTemplate>
            </asp:TemplateField>

            <asp:TemplateField>
                <HeaderTemplate>
                    January
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Label Text='<%# Eval("JANUARY") %>' ForeColor='<%# GetStatusString1(Eval("Target").ToString(),Eval("JANUARY").ToString(),Eval("UPPER_LIMIT_COLOUR").ToString(),Eval("LOWER_LIMIT_COLOUR").ToString())%>' runat="server" />
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="txtJanuary" Text='<%# Eval("JANUARY") %>' Enabled='<%# ActiveMonth(1)%>' runat="server" />
                </EditItemTemplate>
                <FooterTemplate>
                    <asp:TextBox ID="txtJanuaryFooter" runat="server" />
                </FooterTemplate>
            </asp:TemplateField>

            <asp:TemplateField>
                <HeaderTemplate>
                    February
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Label Text='<%# Eval("FEBRUARY") %>' runat="server" ForeColor='<%# GetStatusString1(Eval("Target").ToString(),Eval("FEBRUARY").ToString(),Eval("UPPER_LIMIT_COLOUR").ToString(),Eval("LOWER_LIMIT_COLOUR").ToString())%>' />
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="txtFebruary" Text='<%# Eval("FEBRUARY") %>' Enabled='<%# ActiveMonth(2)%>' runat="server" />
                </EditItemTemplate>
                <FooterTemplate>
                    <asp:TextBox ID="txtFebruaryFooter" runat="server" />
                </FooterTemplate>
            </asp:TemplateField>

            <asp:TemplateField>
                <HeaderTemplate>
                    March
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Label Text='<%# Eval("MARCH") %>' runat="server" ForeColor='<%# GetStatusString1(Eval("Target").ToString(),Eval("MARCH").ToString(),Eval("UPPER_LIMIT_COLOUR").ToString(),Eval("LOWER_LIMIT_COLOUR").ToString())%>' />
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="txtMarch" Text='<%# Eval("MARCH") %>' Enabled='<%# ActiveMonth(3)%>' runat="server" />
                </EditItemTemplate>
                <FooterTemplate>
                    <asp:TextBox ID="txtMarchFooter" runat="server" />
                </FooterTemplate>
            </asp:TemplateField>

            <asp:TemplateField>
                <HeaderTemplate>
                    April
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Label Text='<%# Eval("APRIL") %>' runat="server" ForeColor='<%# GetStatusString1(Eval("Target").ToString(),Eval("APRIL").ToString(),Eval("UPPER_LIMIT_COLOUR").ToString(),Eval("LOWER_LIMIT_COLOUR").ToString())%>' />
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="txtApril" Text='<%# Eval("APRIL") %>' Enabled='<%# ActiveMonth(4)%>' runat="server" />
                </EditItemTemplate>
                <FooterTemplate>
                    <asp:TextBox ID="txtAprilFooter" runat="server" />
                </FooterTemplate>
            </asp:TemplateField>

            <asp:TemplateField>
                <HeaderTemplate>
                    May
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Label Text='<%# Eval("MAY") %>' runat="server" ForeColor='<%# GetStatusString1(Eval("Target").ToString(),Eval("MAY").ToString(),Eval("UPPER_LIMIT_COLOUR").ToString(),Eval("LOWER_LIMIT_COLOUR").ToString())%>' />
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="txtMay" Text='<%# Eval("MAY") %>' Enabled='<%# ActiveMonth(5)%>' runat="server" />
                </EditItemTemplate>
                <FooterTemplate>
                    <asp:TextBox ID="txtMayFooter" runat="server" />
                </FooterTemplate>
            </asp:TemplateField>

            <asp:TemplateField>
                <HeaderTemplate>
                    June
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Label Text='<%# Eval("JUNE") %>' runat="server" ForeColor='<%# GetStatusString1(Eval("Target").ToString(),Eval("JUNE").ToString(),Eval("UPPER_LIMIT_COLOUR").ToString(),Eval("LOWER_LIMIT_COLOUR").ToString())%>' />
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="txtJune" Text='<%# Eval("JUNE") %>' Enabled='<%# ActiveMonth(6)%>' runat="server" />
                </EditItemTemplate>
                <FooterTemplate>
                    <asp:TextBox ID="txtJuneFooter" runat="server" />
                </FooterTemplate>
            </asp:TemplateField>

            <asp:TemplateField>
                <HeaderTemplate>
                    July
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Label Text='<%# Eval("JULY") %>' runat="server" ForeColor='<%# GetStatusString1(Eval("Target").ToString(),Eval("JULY").ToString(),Eval("UPPER_LIMIT_COLOUR").ToString(),Eval("LOWER_LIMIT_COLOUR").ToString())%>' />
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="txtJuly" Text='<%# Eval("JULY") %>' Enabled='<%# ActiveMonth(7)%>' runat="server" />
                </EditItemTemplate>
                <FooterTemplate>
                    <asp:TextBox ID="txtJulyFooter" runat="server" />
                </FooterTemplate>
            </asp:TemplateField>

            <asp:TemplateField>
                <HeaderTemplate>
                    August
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Label Text='<%# Eval("AUGUST") %>' runat="server" ForeColor='<%# GetStatusString1(Eval("Target").ToString(),Eval("AUGUST").ToString(),Eval("UPPER_LIMIT_COLOUR").ToString(),Eval("LOWER_LIMIT_COLOUR").ToString())%>' />
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="txtAugust" Text='<%# Eval("AUGUST") %>' Enabled='<%# ActiveMonth(8)%>' runat="server" />
                </EditItemTemplate>
                <FooterTemplate>
                    <asp:TextBox ID="txtAugustFooter" runat="server" />
                </FooterTemplate>
            </asp:TemplateField>

            <asp:TemplateField>
                <HeaderTemplate>
                    September
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Label Text='<%# Eval("SEPTEMBER") %>' runat="server" ForeColor='<%# GetStatusString1(Eval("Target").ToString(),Eval("SEPTEMBER").ToString(),Eval("UPPER_LIMIT_COLOUR").ToString(),Eval("LOWER_LIMIT_COLOUR").ToString())%>' />
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="txtSeptember" Text='<%# Eval("SEPTEMBER") %>' Enabled='<%# ActiveMonth(9)%>' runat="server" />
                </EditItemTemplate>
                <FooterTemplate>
                    <asp:TextBox ID="txtSeptemberFooter" runat="server" />
                </FooterTemplate>
            </asp:TemplateField>

            <asp:TemplateField>
                <HeaderTemplate>
                    Year To Date
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Label Text='<%# Eval("YTD") %>' runat="server" ForeColor='<%# GetStatusString1(Eval("Target").ToString(),Eval("YTD").ToString(),Eval("UPPER_LIMIT_COLOUR").ToString(),Eval("LOWER_LIMIT_COLOUR").ToString())%>' />
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="txtYTD" Text='<%# Eval("YTD") %>' runat="server" />
                </EditItemTemplate>
                <FooterTemplate>
                    <asp:TextBox ID="txtYTDFooter" runat="server" />
                </FooterTemplate>
            </asp:TemplateField>

        </Columns>

        <HeaderStyle CssClass="GridviewScrollHeader" />
        <RowStyle CssClass="GridviewScrollItem" />
        <PagerStyle CssClass="GridviewScrollPager" />

    </asp:GridView>
               <%--    </asp:PlaceHolder>
         </div>     
      </asp:Panel>  --%>
     </ContentTemplate> 

    </asp:UpdatePanel>

     <h1 align="center"><asp:Label ID="lblNoData" runat="server" Text="No Data Found" ForeColor="Red" Visible="false"></asp:Label> </h1>

   

    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();

        prm.add_endRequest(function () {
            $(document).ready(function () {
                gridviewScroll();
            });
        });

        $(document).ready(function () {
            gridviewScroll();
        });
        function gridviewScroll() {
            var viewPortWidth = $(window).width();
            var viewPortHeight = $(window).height();

            //// the more standards compliant browsers (mozilla/netscape/opera/IE7) use window.innerWidth and window.innerHeight
            //if (typeof window.innerWidth != 'undefined') {
            //    viewPortWidth = window.innerWidth,
            //        viewPortHeight = window.innerHeight
            //}

            //// IE6 in standards compliant mode (i.e. with a valid doctype as the first line in the document)
            //else if (typeof document.documentElement != 'undefined'
            //    && typeof document.documentElement.clientWidth !=
            //    'undefined' && document.documentElement.clientWidth != 0) {
            //    viewPortWidth = document.documentElement.clientWidth,
            //        viewPortHeight = document.documentElement.clientHeight
            //}

            //// older versions of IE
            //else {
            //    viewPortWidth = document.getElementsByTagName('body')[0].clientWidth,
            //        viewPortHeight = document.getElementsByTagName('body')[0].clientHeight
            //}

            $('#<%=gvDisplay_ScoreBoard.ClientID%>').gridviewScroll({
                width: viewPortWidth- 25/*screen.availWidth-40*//*1175*/,
                height: viewPortHeight - 105/*screen.availHeight-290*//*500*/,
                //freezesize: 0,
                barcolor: '#044E83',
                arrowsize: 15,
                varrowtopimg: "../Content/Images/ArrowTop.png",
                varrowbottomimg: "../Content/Images/ArrowBottom.png",
                harrowleftimg: "../Content/Images/ArrowLeft.png",
                harrowrightimg: "../Content/Images/ArrowRight.png",

                startVertical: $("#<%=hfGridView1SV.ClientID%>").val(),
                startHorizontal: $("#<%=hfGridView1SH.ClientID%>").val(),
                onScrollVertical: function (delta) {
                    $("#<%=hfGridView1SV.ClientID%>").val(delta);
                },
                onScrollHorizontal: function (delta) {
                    $("#<%=hfGridView1SH.ClientID%>").val(delta);
                }
            });
        }
    </script>
    <asp:HiddenField ID="hfGridView1SV" runat="server" /> 
<asp:HiddenField ID="hfGridView1SH" runat="server" />
    
    

</asp:Content>
