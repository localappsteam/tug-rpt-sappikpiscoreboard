﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Sappi_Kpi_ScoreBoard.Webpages
{
    public partial class BuildDashBoard_CRUD_KpiScoreBoard : System.Web.UI.Page
    {
        Classes.DBBackend DBBackend = new Classes.DBBackend();
        Classes.Checks Checks = new Classes.Checks();
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                redirect();
                populateGV_GroupName();
                populateGV_Kpi();
            }
        }

        protected void redirect()
        {
            if (Checks.CheckForAccess() == true)
                Response.Redirect("DisplayDashBoard_KpiScoreBoard.aspx");
        }
        private void populateGV_GroupName()
        {
            var dt = new DataTable();
            DataRow dr = null;
            dt = DBBackend.gv_GroupNames();
            
            if (dt.Rows.Count > 0)
            {
                gvDashBoardGroups.DataSource = dt;
                gvDashBoardGroups.DataBind();
                txtGroupID.Text = dt.Rows[0]["Group_ID"].ToString();
                lblGroupNameDetails.Text= dt.Rows[0]["Group_Name"].ToString();
            }
            else
            {
                 dr = dt.NewRow();
               
                dr["Group_id"] = 1;
                dr["Group_name"] = "";
                dr["Group_Year"] = 0000;
                dr["setorder"] = 0;
                dr["display"] = "Y";

                dt.Rows.Add(dr);
                gvDashBoardGroups.DataSource = dt;
                gvDashBoardGroups.DataBind();
                gvDashBoardGroups.Rows[0].Cells.Clear();
                gvDashBoardGroups.Rows[0].Cells.Add(new TableCell());
                gvDashBoardGroups.Rows[0].Cells[0].ColumnSpan = dt.Columns.Count;
                gvDashBoardGroups.Rows[0].Cells[0].Text = "No Data Found..!";
                gvDashBoardGroups.Rows[0].Cells[0].HorizontalAlign = HorizontalAlign.Center;
            }
            populateGV_Kpi();
        }

        private void populateGV_Kpi()
        {
            try
            {

                var dt = new DataTable();
                DataRow dr = null;
                dt = DBBackend.gv_KPI_Definition(Convert.ToInt64(txtGroupID.Text));

                if (dt.Rows.Count > 0)
                {
                    gvKPI.DataSource = dt;
                    gvKPI.DataBind();
                    lblGroupNameDetails.Text = dt.Rows[0]["Group_Name"].ToString();
                }
                else
                {
                    dr = dt.NewRow();
                    dr["Kpi_id"] = 0;
                    dr["Group_id"] = 1;
                    dr["Group_name"] = "";
                    dr["setorder"] = 0;
                    dr["display"] = "Y";

                    dt.Rows.Add(dr);
                    gvKPI.DataSource = dt;
                    gvKPI.DataBind();
                    gvKPI.Rows[0].Cells.Clear();
                    gvKPI.Rows[0].Cells.Add(new TableCell());
                    gvKPI.Rows[0].Cells[0].ColumnSpan = dt.Columns.Count;
                    gvKPI.Rows[0].Cells[0].Text = "No Data Found..!";
                    gvKPI.Rows[0].Cells[0].HorizontalAlign = HorizontalAlign.Center;
                    // lblGroupNameDetails.Text = "";
                }
            }
            catch (Exception)
            {

                // throw;
            }
        }

        protected void gvDashBoardGroups_SelectedIndexChanged(object sender, EventArgs e)
        {
           // var groupid = gvDashBoardGroups.DataKeys[e.].Value.ToString();
        }

        protected void gvDashBoardGroups_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("AddNew"))
            {
                  try
                  {

                var groupname = (gvDashBoardGroups.FooterRow.FindControl("txtGroupNameFooter") as TextBox).Text.Trim();
                var groupyear =(gvDashBoardGroups.FooterRow.FindControl("txtGroupYearFooter") as DropDownList).Text.Trim();
                var orderby =(gvDashBoardGroups.FooterRow.FindControl("txtSetOrderFooter") as DropDownList).Text.Trim();
                var display =(gvDashBoardGroups.FooterRow.FindControl("txtDisplayFooter") as DropDownList).Text.Trim();
               
                    DBBackend.Capture_GroupName(groupname,Convert.ToInt64(groupyear),Convert.ToInt64(orderby),display);

                    lblSuccessMessage.Text="Data Added";
                    lblError.Text = "";
                 populateGV_GroupName();
                }
                catch (Exception ex)
                {
                    lblSuccessMessage.Text = "";
                                       lblError.Text = ex.Message;
                }

            }
        }

        protected void gvDashBoardGroups_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gvDashBoardGroups.EditIndex = e.NewEditIndex;
            populateGV_GroupName();
        }

        protected void gvDashBoardGroups_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gvDashBoardGroups.EditIndex =-1;
            populateGV_GroupName();
        }

        protected void gvDashBoardGroups_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
        
                try
                {
                    var groupid = gvDashBoardGroups.DataKeys[e.RowIndex].Value.ToString();
                    var groupname = (gvDashBoardGroups.Rows[e.RowIndex].FindControl("txtGroupName") as TextBox).Text.Trim();
                    var groupyear = (gvDashBoardGroups.Rows[e.RowIndex].FindControl("txtGroupYear") as DropDownList).Text.Trim();
                    var orderby = (gvDashBoardGroups.Rows[e.RowIndex].FindControl("txtSetOrder") as DropDownList).Text.Trim();
                    var display = (gvDashBoardGroups.Rows[e.RowIndex].FindControl("txtDisplay") as DropDownList).Text.Trim();

                    DBBackend.Update_GroupName(Convert.ToInt64(groupid), groupname,Convert.ToInt64(groupyear), Convert.ToInt64(orderby), display);


                    lblSuccessMessage.Text = "Selected Row Updated";
                    lblError.Text = "";
                gvDashBoardGroups.EditIndex = -1;
                populateGV_GroupName();

                }
                catch (Exception ex)
                {
                  lblSuccessMessage.Text = "";
                   lblError.Text = ex.Message;
                }
        }
//********************************************KPI*******************************************************
        protected void gvKPI_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gvKPI.EditIndex = e.NewEditIndex;
            populateGV_Kpi();
        }

        protected void gvKPI_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("AddNew"))
            {
                try
                {


                    var txtObjectiveFooter = (gvKPI.FooterRow.FindControl("txtObjectiveFooter") as TextBox).Text.Trim();
                    var txtKPIFooter = (gvKPI.FooterRow.FindControl("txtKPIFooter") as TextBox).Text.Trim();
                    var txtRESPFooter = (gvKPI.FooterRow.FindControl("txtRESPFooter") as TextBox).Text.Trim();
                    var txtUOMFooter = (gvKPI.FooterRow.FindControl("txtUOMFooter") as TextBox).Text.Trim();
                    var txtTargetFooter = (gvKPI.FooterRow.FindControl("txtTargetFooter") as TextBox).Text.Trim();
                    var txtKpiSetOrderFooter = (gvKPI.FooterRow.FindControl("txtKpiSetOrderFooter") as DropDownList).Text.Trim();
                    var txtKpiDisplayFooter = (gvKPI.FooterRow.FindControl("txtKpiDisplayFooter") as DropDownList).Text.Trim();
                    var txtMonthOrYearFooter = (gvKPI.FooterRow.FindControl("txtMonthOrYearFooter") as DropDownList).Text.Trim();
                    var txtUpperLimitColour = (gvKPI.FooterRow.FindControl("txtUpperLimitColourFooter") as DropDownList).Text.Trim();
                    var txtLowerLimitColour = (gvKPI.FooterRow.FindControl("txtLowerLimitColourFooter") as DropDownList).Text.Trim();

                    DBBackend.Capture_KPI_Definitions(Convert.ToInt64(txtGroupID.Text),
                                                    txtObjectiveFooter, txtKPIFooter,
                                                    txtRESPFooter, txtUOMFooter,
                                                    Checks.checkIfNull(txtTargetFooter),
                                                    Convert.ToInt64(txtKpiSetOrderFooter), 
                                                    txtKpiDisplayFooter,
                                                    txtKpiDisplayFooter, 
                                                    txtUpperLimitColour, 
                                                    txtLowerLimitColour);


                    lblKpiSuccess.Text = "Data Added";
                    lblKpiError.Text = "";
                   populateGV_Kpi();
                }
                catch (InvalidCastException ex)
                {
                    lblKpiSuccess.Text = "";
                    lblKpiError.Text = ex.Message;
                    populateGV_Kpi();
                }

            }
        }

        protected void gvKPI_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gvKPI.EditIndex = -1;
            populateGV_Kpi();
        }

        protected void gvKPI_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                //var groupid = gvDashBoardGroups.DataKeys[e.RowIndex].Value.ToString();
                //var groupname = (gvDashBoardGroups.Rows[e.RowIndex].FindControl("txtGroupName") as TextBox).Text.Trim();
            var kpi_id = gvKPI.DataKeys[e.RowIndex].Value.ToString();
            var txtObjective = (gvKPI.Rows[e.RowIndex].FindControl("txtObjective") as TextBox).Text.Trim();
            var txtKPI = (gvKPI.Rows[e.RowIndex].FindControl("txtKPI") as TextBox).Text.Trim();
            var txtRESP = (gvKPI.Rows[e.RowIndex].FindControl("txtRESP") as TextBox).Text.Trim();
            var txtUOM = (gvKPI.Rows[e.RowIndex].FindControl("txtUOM") as TextBox).Text.Trim();
            var txtTarget = (gvKPI.Rows[e.RowIndex].FindControl("txtTarget") as TextBox).Text.Trim();
            var txtKpiSetOrder = (gvKPI.Rows[e.RowIndex].FindControl("txtKpiSetOrder") as DropDownList).Text.Trim();
            var txtKpiDisplay= (gvKPI.Rows[e.RowIndex].FindControl("txtKpiDisplay") as DropDownList).Text.Trim();
            var txtMonthOrYear = (gvKPI.Rows[e.RowIndex].FindControl("txtMonthOrYear") as DropDownList).Text.Trim();
            var txtUpperLimitColour = (gvKPI.Rows[e.RowIndex].FindControl("txtUpperLimitColour") as DropDownList).Text.Trim();
            var txtLowerLimitColour = (gvKPI.Rows[e.RowIndex].FindControl("txtLowerLimitColour") as DropDownList).Text.Trim();


                DBBackend.Update_KPI_Definitions(Convert.ToInt64(kpi_id),
                                               Convert.ToInt64(txtGroupID.Text),
                                               txtObjective,
                                               txtKPI,
                                               txtRESP,
                                               txtUOM,
                                               Checks.checkIfNull(txtTarget),
                                               Convert.ToInt64(txtKpiSetOrder),
                                               txtKpiDisplay,
                                               txtMonthOrYear,
                                               txtUpperLimitColour,
                                               txtLowerLimitColour);


            lblKpiSuccess.Text = "Selected Row Updated";
            lblKpiError.Text = "";
                gvKPI.EditIndex = -1;
                populateGV_Kpi();

        }
                catch (Exception ex)
                {
                    lblKpiSuccess.Text = "";
                    lblKpiError.Text = ex.Message;
                }
}

        protected void gvDashBoardGroups_RowDataBound(object sender, GridViewRowEventArgs e)
        {
           // var groupid = gvDashBoardGroups.DataKeys[e.RowIndex].Value.ToString();
        }

        protected void lnkSelect_Click(object sender, EventArgs e)
        {
            // var rptid = new EngShiftReport.EngShiftReportEditIncident();
            // Int64 group_id = Convert.ToInt64((sender as LinkButton).CommandArgument);

            LinkButton _sender = (LinkButton)sender;
            string Id = ((LinkButton)sender).CommandArgument.ToString();
            string[] Data = Id.Split(';');
            string GPID = Data[0].ToString();
            string GPN = Data[1].ToString();

            txtGroupID.Text = GPID;
            lblGroupNameDetails.Text = GPN;

            populateGV_Kpi();
            //Server.Transfer(FormatUrl(reportId));//+"SappiEngShiftReportCaptureIncidents.aspx");
            // Response.Redirect("SappiEngShiftReportTechFeedBack.aspx?ReportID=" + reportid.ToString());

           
        }
    }
}