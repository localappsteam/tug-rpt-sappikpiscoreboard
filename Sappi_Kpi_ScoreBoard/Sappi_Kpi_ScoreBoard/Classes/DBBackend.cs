﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using Oracle.ManagedDataAccess;
using Oracle.ManagedDataAccess.Client;
using System.Data.SqlClient;
using System.Threading.Tasks;
using System.DirectoryServices.AccountManagement;
using System.DirectoryServices;
//using System.DirectoryServices.AccountManagement;
//using System.DirectoryServices;




namespace Sappi_Kpi_ScoreBoard.Classes
{
  
        public class DBBackend 
    {
       string CheckCurrentIdentity = HttpContext.Current.User.Identity.Name.Replace("SAPPISA\\", "").ToUpper().Trim();
        public void Capture_GroupName( string i_group_name, long i_group_year, long i_setorder,string i_display)
            {

            //PCK_KPISCOREBOARD.Create_GroupName(i_group_name => :i_group_name,
            //                         i_group_year => :i_group_year,
            //                         i_setorder => :i_setorder,
            //                         i_display => :i_display);

            Capture_Log("", "", "", CheckCurrentIdentity, i_group_name, "Create Group Name");

            var conn = new OracleConnection(OraConnectionString());

                var cmd = new OracleCommand("PCK_KPISCOREBOARD.Create_GroupName", conn)
                {
                    CommandType = CommandType.StoredProcedure,
                    BindByName = true
                };

                OracleParameter cp = new OracleParameter
                {
                    ParameterName = "i_group_name",
                    Size = 200,
                    OracleDbType = OracleDbType.Varchar2,
                    Direction = ParameterDirection.Input,
                    Value = i_group_name
                };
                cmd.Parameters.Add(cp);

            OracleParameter cp1 = new OracleParameter
            {
                ParameterName = "i_group_year",
                Size = 200,
                OracleDbType = OracleDbType.Int64,
                Direction = ParameterDirection.Input,
                Value =  i_group_year
            };
            cmd.Parameters.Add(cp1);

            OracleParameter cp2 = new OracleParameter
            {
                ParameterName = "i_setorder",
                Size = 200,
                OracleDbType = OracleDbType.Int64,
                Direction = ParameterDirection.Input,
                Value = i_setorder
            };
            cmd.Parameters.Add(cp2);

            OracleParameter cp3 = new OracleParameter
            {
                ParameterName = "i_display",
                Size = 200,
                OracleDbType = OracleDbType.Varchar2,
                Direction = ParameterDirection.Input,
                Value = i_display
            };
            cmd.Parameters.Add(cp3);


            cmd.Connection.Open();
                cmd.ExecuteScalar();
                cmd.Connection.Close();
                cmd.Dispose();

             
            }
        public void Update_GroupName(long i_group_id, string i_group_name, long i_group_year, long i_setorder, string i_display)
        {

            //PCK_KPISCOREBOARD.Update_GroupName(i_group_id => :i_group_id,
            //                         i_group_name => :i_group_name,
            //                         i_group_year => :i_group_year,
            //                         i_setorder => :i_setorder,
            //                         i_display => :i_display);

            Capture_Log(i_group_id.ToString(), "", "", CheckCurrentIdentity, i_group_name, "Update Group Name");

            var conn = new OracleConnection(OraConnectionString());

            var cmd = new OracleCommand("PCK_KPISCOREBOARD.Update_GroupName", conn)
            {
                CommandType = CommandType.StoredProcedure,
                BindByName = true
            };

            OracleParameter cp = new OracleParameter
            {
                ParameterName = "i_group_name",
                Size = 200,
                OracleDbType = OracleDbType.Varchar2,
                Direction = ParameterDirection.Input,
                Value = i_group_name
            };
            cmd.Parameters.Add(cp);

            OracleParameter cp1 = new OracleParameter
            {
                ParameterName = "i_group_year",
                Size = 200,
                OracleDbType = OracleDbType.Int64,
                Direction = ParameterDirection.Input,
                Value = i_group_year
            };
            cmd.Parameters.Add(cp1);

            OracleParameter cp2 = new OracleParameter
            {
                ParameterName = "i_setorder",
                Size = 200,
                OracleDbType = OracleDbType.Int64,
                Direction = ParameterDirection.Input,
                Value = i_setorder
            };
            cmd.Parameters.Add(cp2);

            OracleParameter cp3 = new OracleParameter
            {
                ParameterName = "i_display",
                Size = 200,
                OracleDbType = OracleDbType.Varchar2,
                Direction = ParameterDirection.Input,
                Value = i_display
            };
            cmd.Parameters.Add(cp3);

            OracleParameter cp4 = new OracleParameter
            {
                ParameterName = "i_group_id",
                Size = 200,
                OracleDbType = OracleDbType.Int64,
                Direction = ParameterDirection.Input,
                Value = i_group_id
            };
            cmd.Parameters.Add(cp4);

            cmd.Connection.Open();
            cmd.ExecuteScalar();
            cmd.Connection.Close();
            cmd.Dispose();


        }
        public void Capture_KPI_Definitions(long i_group_id, string i_objective, string i_kpi, string i_resp, string i_uom ,double? i_target, long i_setorder,string i_display ,string i_month_or_year ,string i_upper_limit_colour, string i_lower_limit_colour)
        {

            //PCK_KPISCOREBOARD.Create_KPI_Definition(i_group_id => :i_group_id,
            //                              i_objective => :i_objective,
            //                              i_kpi => :i_kpi,
            //                              i_resp => :i_resp,
            //                              i_uom => :i_uom,
            //                              i_target => :i_target,
            //                              i_setorder => :i_setorder,
            //                              i_display => :i_display);

            Capture_Log(i_group_id.ToString(), "", "", CheckCurrentIdentity, i_objective+" - " + i_kpi , "Capture KPI Definitions");

            var conn = new OracleConnection(OraConnectionString());

            var cmd = new OracleCommand("PCK_KPISCOREBOARD.Create_KPI_Definition", conn)
            {
                CommandType = CommandType.StoredProcedure,
                BindByName = true
            };

            OracleParameter cp = new OracleParameter
            {
                ParameterName = "i_group_id",
                Size = 200,
                OracleDbType = OracleDbType.Int64,
                Direction = ParameterDirection.Input,
                Value = i_group_id
            };
            cmd.Parameters.Add(cp);

            OracleParameter cp1 = new OracleParameter
            {
                ParameterName = "i_objective",
                Size = 200,
                OracleDbType = OracleDbType.Varchar2,
                Direction = ParameterDirection.Input,
                Value = i_objective
            };
            cmd.Parameters.Add(cp1);

            OracleParameter cp2 = new OracleParameter
            {
                ParameterName = "i_kpi",
                Size = 200,
                OracleDbType = OracleDbType.Varchar2,
                Direction = ParameterDirection.Input,
                Value = i_kpi
            };
            cmd.Parameters.Add(cp2);

            OracleParameter cp3 = new OracleParameter
            {
                ParameterName = "i_resp",
                Size = 200,
                OracleDbType = OracleDbType.Varchar2,
                Direction = ParameterDirection.Input,
                Value = i_resp
            };
            cmd.Parameters.Add(cp3);

            OracleParameter cp4 = new OracleParameter
            {
                ParameterName = "i_uom",
                Size = 200,
                OracleDbType = OracleDbType.Varchar2,
                Direction = ParameterDirection.Input,
                Value = i_uom
            };
            cmd.Parameters.Add(cp4);

            OracleParameter cp6 = new OracleParameter
            {
                ParameterName = "i_target",
                Size = 200,
                OracleDbType = OracleDbType.Double,
                Direction = ParameterDirection.Input,
                Value = i_target
            };
            cmd.Parameters.Add(cp6);

            OracleParameter cp7 = new OracleParameter
            {
                ParameterName = "i_setorder",
                Size = 200,
                OracleDbType = OracleDbType.Int64,
                Direction = ParameterDirection.Input,
                Value = i_setorder
            };
            cmd.Parameters.Add(cp7);

            OracleParameter cp8 = new OracleParameter
            {
                ParameterName = "i_display",
                Size = 200,
                OracleDbType = OracleDbType.Varchar2,
                Direction = ParameterDirection.Input,
                Value = i_display
            };
            cmd.Parameters.Add(cp8);

            OracleParameter cp9 = new OracleParameter
            {
                ParameterName = "i_month_or_year",
                Size = 200,
                OracleDbType = OracleDbType.Varchar2,
                Direction = ParameterDirection.Input,
                Value = i_month_or_year
            };
            cmd.Parameters.Add(cp9);

            OracleParameter cp10 = new OracleParameter
            {
                ParameterName = " i_upper_limit_colour",
                Size = 200,
                OracleDbType = OracleDbType.Varchar2,
                Direction = ParameterDirection.Input,
                Value = i_upper_limit_colour
            };
            cmd.Parameters.Add(cp10);

            OracleParameter cp11 = new OracleParameter
            {
                ParameterName = " i_lower_limit_colour",
                Size = 200,
                OracleDbType = OracleDbType.Varchar2,
                Direction = ParameterDirection.Input,
                Value = i_lower_limit_colour
            };
            cmd.Parameters.Add(cp11);

            cmd.Connection.Open();
            cmd.ExecuteScalar();
            cmd.Connection.Close();
            cmd.Dispose();


        }
        public void Update_KPI_Definitions(long i_kpi_id, long i_group_id, string i_objective, string i_kpi, string i_resp, string i_uom, double? i_target, long i_setorder, string i_display ,string i_month_or_year,string i_upper_limit_colour, string i_lower_limit_colour)
        {

            //PCK_KPISCOREBOARD.Update_KPI_Definition(i_kpi_id => :i_kpi_id,
            //                              i_objective => :i_objective,
            //                              i_kpi => :i_kpi,
            //                              i_resp => :i_resp,
            //                              i_uom => :i_uom,
            //                              i_target => :i_target,
            //                              i_setorder => :i_setorder,
            //                              i_display => :i_display);

            Capture_Log(i_group_id.ToString(), i_kpi_id.ToString(), "", CheckCurrentIdentity, i_objective + " - " + i_kpi, "Update KPI Definitions");

            var conn = new OracleConnection(OraConnectionString());

            var cmd = new OracleCommand("PCK_KPISCOREBOARD.Update_KPI_Definition", conn)
            {
                CommandType = CommandType.StoredProcedure,
                BindByName = true
            };

            //OracleParameter cp = new OracleParameter
            //{
            //    ParameterName = "i_group_id",
            //    Size = 200,
            //    OracleDbType = OracleDbType.Int64,
            //    Direction = ParameterDirection.Input,
            //    Value = i_group_id
            //};
            //cmd.Parameters.Add(cp);

            OracleParameter cp1 = new OracleParameter
            {
                ParameterName = "i_objective",
                Size = 200,
                OracleDbType = OracleDbType.Varchar2,
                Direction = ParameterDirection.Input,
                Value = i_objective
            };
            cmd.Parameters.Add(cp1);

            OracleParameter cp2 = new OracleParameter
            {
                ParameterName = "i_kpi",
                Size = 200,
                OracleDbType = OracleDbType.Varchar2,
                Direction = ParameterDirection.Input,
                Value = i_kpi
            };
            cmd.Parameters.Add(cp2);

            OracleParameter cp3 = new OracleParameter
            {
                ParameterName = "i_resp",
                Size = 200,
                OracleDbType = OracleDbType.Varchar2,
                Direction = ParameterDirection.Input,
                Value = i_resp
            };
            cmd.Parameters.Add(cp3);

            OracleParameter cp4 = new OracleParameter
            {
                ParameterName = "i_uom",
                Size = 200,
                OracleDbType = OracleDbType.Varchar2,
                Direction = ParameterDirection.Input,
                Value = i_uom
            };
            cmd.Parameters.Add(cp4);

            OracleParameter cp6 = new OracleParameter
            {
                ParameterName = "i_target",
                Size = 200,
                OracleDbType = OracleDbType.Double,
                Direction = ParameterDirection.Input,
                Value = i_target
            };
            cmd.Parameters.Add(cp6);

            OracleParameter cp7 = new OracleParameter
            {
                ParameterName = "i_setorder",
                Size = 200,
                OracleDbType = OracleDbType.Int64,
                Direction = ParameterDirection.Input,
                Value = i_setorder
            };
            cmd.Parameters.Add(cp7);

            OracleParameter cp8 = new OracleParameter
            {
                ParameterName = "i_display",
                Size = 200,
                OracleDbType = OracleDbType.Varchar2,
                Direction = ParameterDirection.Input,
                Value = i_display
            };
            cmd.Parameters.Add(cp8);

            OracleParameter cp9 = new OracleParameter
            {
                ParameterName = "i_kpi_id",
                Size = 200,
                OracleDbType = OracleDbType.Varchar2,
                Direction = ParameterDirection.Input,
                Value = i_kpi_id
            };
            cmd.Parameters.Add(cp9);

            OracleParameter cp10 = new OracleParameter
            {
                ParameterName = "i_month_or_year",
                Size = 200,
                OracleDbType = OracleDbType.Varchar2,
                Direction = ParameterDirection.Input,
                Value = i_month_or_year
            };
            cmd.Parameters.Add(cp10);

            OracleParameter cp11 = new OracleParameter
            {
                ParameterName = " i_upper_limit_colour",
                Size = 200,
                OracleDbType = OracleDbType.Varchar2,
                Direction = ParameterDirection.Input,
                Value = i_upper_limit_colour
            };
            cmd.Parameters.Add(cp11);

            OracleParameter cp12 = new OracleParameter
            {
                ParameterName = " i_lower_limit_colour",
                Size = 200,
                OracleDbType = OracleDbType.Varchar2,
                Direction = ParameterDirection.Input,
                Value = i_lower_limit_colour
            };
            cmd.Parameters.Add(cp12);

            cmd.Connection.Open();
            cmd.ExecuteScalar();
            cmd.Connection.Close();
            cmd.Dispose();


        }
        public void Update_KPI_Results(long i_kpi_id, double? i_january, double? i_february , double? i_march , double? i_april , double? i_may , double? i_june , double? i_july, double? i_august, double? i_september, double? i_october, double? i_november, double? i_december,Double? i_ytd)

        {
            //PCK_KPISCOREBOARD.Update_KPI_Results(i_kpi_id => :i_kpi_id,
            //                           i_january => :i_january,
            //                           i_february => :i_february,
            //                           i_march => :i_march,
            //                           i_april => :i_april,
            //                           i_may => :i_may,
            //                           i_june => :i_june,
            //                           i_july => :i_july,
            //                           i_august => :i_august,
            //                           i_september => :i_september,
            //                           i_october => :i_october,
            //                           i_november => :i_november,
            //                           i_december => :i_december);

            Capture_Log("", i_kpi_id.ToString(), "", CheckCurrentIdentity,"J"+ i_january + "F" + i_february + "M" + i_march + "A" + i_april + "M" + i_may + "J" + i_june + "J" + i_july + "A" + i_august + "S" + i_september + "O" + i_october + "N" + i_november + "D" + i_december + "YTD" + i_ytd , "Update KPI Results");

            var conn = new OracleConnection(OraConnectionString());

            var cmd = new OracleCommand("PCK_KPISCOREBOARD.Update_KPI_Results", conn)
            {
                CommandType = CommandType.StoredProcedure,
                BindByName = true
            };

            //OracleParameter cp = new OracleParameter
            //{
            //    ParameterName = "i_group_id",
            //    Size = 200,
            //    OracleDbType = OracleDbType.Int64,
            //    Direction = ParameterDirection.Input,
            //    Value = i_group_id
            //};
            //cmd.Parameters.Add(cp);

            OracleParameter cp1 = new OracleParameter
            {
                ParameterName = "i_kpi_id",
                Size = 200,
                OracleDbType = OracleDbType.Int64,
                Direction = ParameterDirection.Input,
                Value = i_kpi_id
            };
            cmd.Parameters.Add(cp1);

            OracleParameter cp2 = new OracleParameter
            {
                ParameterName = "i_january",
                Size = 200,
                OracleDbType = OracleDbType.Double,
                Direction = ParameterDirection.Input,
                Value = i_january
            };
            cmd.Parameters.Add(cp2);

            OracleParameter cp3 = new OracleParameter
            {
                ParameterName = "i_february",
                Size = 200,
                OracleDbType = OracleDbType.Double,
                Direction = ParameterDirection.Input,
                Value = i_february
            };
            cmd.Parameters.Add(cp3);

            OracleParameter cp4 = new OracleParameter
            {
                ParameterName = "i_march",
                Size = 200,
                OracleDbType = OracleDbType.Double,
                Direction = ParameterDirection.Input,
                Value = i_march
            };
            cmd.Parameters.Add(cp4);

            OracleParameter cp5 = new OracleParameter
            {
                ParameterName = "i_april",
                Size = 200,
                OracleDbType = OracleDbType.Double,
                Direction = ParameterDirection.Input,
                Value = i_april
            };
            cmd.Parameters.Add(cp5);

            OracleParameter cp6 = new OracleParameter
            {
                ParameterName = "i_may",
                Size = 200,
                OracleDbType = OracleDbType.Double,
                Direction = ParameterDirection.Input,
                Value = i_may
            };
            cmd.Parameters.Add(cp6);

            OracleParameter cp7 = new OracleParameter
            {
                ParameterName = "i_june",
                Size = 200,
                OracleDbType = OracleDbType.Double,
                Direction = ParameterDirection.Input,
                Value = i_june
            };
            cmd.Parameters.Add(cp7);

            OracleParameter cp8 = new OracleParameter
            {
                ParameterName = "i_july",
                Size = 200,
                OracleDbType = OracleDbType.Double,
                Direction = ParameterDirection.Input,
                Value = i_july
            };
            cmd.Parameters.Add(cp8);

            OracleParameter cp9 = new OracleParameter
            {
                ParameterName = "i_august",
                Size = 200,
                OracleDbType = OracleDbType.Double,
                Direction = ParameterDirection.Input,
                Value = i_august
            };
            cmd.Parameters.Add(cp9);

            OracleParameter cp10 = new OracleParameter
            {
                ParameterName = "i_september",
                Size = 200,
                OracleDbType = OracleDbType.Double,
                Direction = ParameterDirection.Input,
                Value = i_september
            };
            cmd.Parameters.Add(cp10);

            OracleParameter cp11 = new OracleParameter
            {
                ParameterName = "i_october",
                Size = 200,
                OracleDbType = OracleDbType.Double,
                Direction = ParameterDirection.Input,
                Value = i_october
            };
            cmd.Parameters.Add(cp11);

            OracleParameter cp12 = new OracleParameter
            {
                ParameterName = "i_november",
                Size = 200,
                OracleDbType = OracleDbType.Double,
                Direction = ParameterDirection.Input,
                Value = i_november
            };
            cmd.Parameters.Add(cp12);

            OracleParameter cp13 = new OracleParameter
            {
                ParameterName = "i_december",
                Size = 200,
                OracleDbType = OracleDbType.Double,
                Direction = ParameterDirection.Input,
                Value = i_december
            };
            cmd.Parameters.Add(cp13);

            OracleParameter cp14 = new OracleParameter
            {
                ParameterName = "i_ytd",
                Size = 200,
                OracleDbType = OracleDbType.Double,
                Direction = ParameterDirection.Input,
                Value = i_ytd
            };
            cmd.Parameters.Add(cp14);

            cmd.Connection.Open();
            cmd.ExecuteScalar();
            cmd.Connection.Close();
            cmd.Dispose();


        }
        public DataTable gv_GroupNames()
            {

                var dt = new DataTable();
                var conn = new OracleConnection(OraConnectionString());

                conn.Open();
                var cmd = new OracleCommand("PCK_KPISCOREBOARD.gv_GroupNames", conn)
                {
                    CommandType = CommandType.StoredProcedure,
                    BindByName = true
                };
                OracleParameter cp1 = new OracleParameter
                {
                    ParameterName = "result",
                    OracleDbType = OracleDbType.RefCursor,
                    Direction = ParameterDirection.ReturnValue,
                    Size = 5000
                };

                cmd.Parameters.Add(cp1);
                var dataReader = cmd.ExecuteReader();
                 dt.Load(dataReader);
                cmd.Connection.Close();
                cmd.Dispose();
                return dt;
            }
        public DataTable gv_KPI_Definition(long i_group_id)
        {
            //PCK_KPISCOREBOARD.gv_KPI_Definition(i_group_id => :i_group_id)

            var dt = new DataTable();
            var conn = new OracleConnection(OraConnectionString());

            conn.Open();
            var cmd = new OracleCommand("PCK_KPISCOREBOARD.gv_KPIDefinition", conn)
            {
                CommandType = CommandType.StoredProcedure,
                BindByName = true
            };

            OracleParameter cp4 = new OracleParameter
            {
                ParameterName = "i_group_id",
                Size = 200,
                OracleDbType = OracleDbType.Int64,
                Direction = ParameterDirection.Input,
                Value = i_group_id //to do
            };
            cmd.Parameters.Add(cp4);


            OracleParameter cp1 = new OracleParameter
            {
                ParameterName = "result",
                OracleDbType = OracleDbType.RefCursor,
                Direction = ParameterDirection.ReturnValue,
                Size = 5000
            };

            cmd.Parameters.Add(cp1);

            var dataReader = cmd.ExecuteReader();
            dt.Load(dataReader);
            cmd.Connection.Close();
            cmd.Dispose();
            return dt;

        }
        public DataTable gv_Display_KpiScoreBoard()
        {
            //PCK_KPISCOREBOARD.gv_Display_KpiScoreBoard

            var dt = new DataTable();
            var conn = new OracleConnection(OraConnectionString());

            conn.Open();
            var cmd = new OracleCommand("PCK_KPISCOREBOARD.gv_DisplayKpiScoreBoard", conn)
            {
                CommandType = CommandType.StoredProcedure,
                BindByName = true
            };

            OracleParameter cp1 = new OracleParameter
            {
                ParameterName = "result",
                OracleDbType = OracleDbType.RefCursor,
                Direction = ParameterDirection.ReturnValue,
                Size = 5000
            };

            cmd.Parameters.Add(cp1);

            var dataReader = cmd.ExecuteReader();
            dt.Load(dataReader);
            cmd.Connection.Close();
            cmd.Dispose();
            return dt;

        }
        public int Get_User_Rights(string i_user_name)
        {
            //PCK_KPISCOREBOARD.Get_User_Rights(i_user_name => :i_user_name);

            var dt = new DataTable();
            var conn = new OracleConnection(OraConnectionString());

            conn.Open();
            var cmd = new OracleCommand("PCK_KPISCOREBOARD.Get_User_Rights", conn)
            {
                CommandType = CommandType.StoredProcedure,
                BindByName = true
            };

            OracleParameter cp4 = new OracleParameter
            {
                ParameterName = "i_user_name",
                Size = 200,
                OracleDbType = OracleDbType.Varchar2,
                Direction = ParameterDirection.Input,
                Value = i_user_name 
            };
            cmd.Parameters.Add(cp4);

            OracleParameter cp1 = new OracleParameter
            {
                ParameterName = "result",
                OracleDbType = OracleDbType.Int32,
                Direction = ParameterDirection.ReturnValue,
                Size = 5000
            };

            cmd.Parameters.Add(cp1);

            cmd.ExecuteNonQuery();
            int retval =Convert.ToInt32(cmd.Parameters["result"].Value.ToString());

            cmd.Connection.Close();
            cmd.Dispose();
            return retval;

        }
        public void Create_User_Access(string i_user_name, string i_access_level)
        {

            //PCK_KPISCOREBOARD.Create_User_Access(i_user_name => :i_user_name,
            //                           i_access_level => :i_access_level);
            Capture_Log("", "", "", CheckCurrentIdentity,"U "+ i_user_name +" A "+ i_access_level, "Create User Access");
            var conn = new OracleConnection(OraConnectionString());

            var cmd = new OracleCommand("PCK_KPISCOREBOARD.Create_User_Access", conn)
            {
                CommandType = CommandType.StoredProcedure,
                BindByName = true
            };

            OracleParameter cp = new OracleParameter
            {
                ParameterName = "i_user_name",
                Size = 200,
                OracleDbType = OracleDbType.Varchar2,
                Direction = ParameterDirection.Input,
                Value = i_user_name
            };
            cmd.Parameters.Add(cp);

            OracleParameter cp1 = new OracleParameter
            {
                ParameterName = "i_access_level",
                Size = 200,
                OracleDbType = OracleDbType.Varchar2,
                Direction = ParameterDirection.Input,
                Value = i_access_level
            };
            cmd.Parameters.Add(cp1);

            cmd.Connection.Open();
            cmd.ExecuteScalar();
            cmd.Connection.Close();
            cmd.Dispose();


        }
        public void Update_User_Access(long i_user_id, string i_user_name, string i_access_level)
        {

            //PCK_KPISCOREBOARD.Update_User_Access(i_user_id => :i_user_id,
            //                           i_user_name => :i_user_name,
            //                           i_access_level => :i_access_level);

           Capture_Log("", "", i_user_id.ToString(), CheckCurrentIdentity, "U " + i_user_name + " A " + i_access_level, "Update User Access");

            var conn = new OracleConnection(OraConnectionString());

            var cmd = new OracleCommand("PCK_KPISCOREBOARD.Update_User_Access", conn)
            {
                CommandType = CommandType.StoredProcedure,
                BindByName = true
            };

            OracleParameter cp = new OracleParameter
            {
                ParameterName = "i_user_id",
                Size = 200,
                OracleDbType = OracleDbType.Int64,
                Direction = ParameterDirection.Input,
                Value = i_user_id
            };
            cmd.Parameters.Add(cp);

            OracleParameter cp1 = new OracleParameter
            {
                ParameterName = "i_user_name",
                Size = 200,
                OracleDbType = OracleDbType.Varchar2,
                Direction = ParameterDirection.Input,
                Value = i_user_name
            };
            cmd.Parameters.Add(cp1);

            OracleParameter cp2 = new OracleParameter
            {
                ParameterName = "i_access_level",
                Size = 200,
                OracleDbType = OracleDbType.Varchar2,
                Direction = ParameterDirection.Input,
                Value = i_access_level
            };
            cmd.Parameters.Add(cp2);

            cmd.Connection.Open();
            cmd.ExecuteScalar();
            cmd.Connection.Close();
            cmd.Dispose();
        }
        public void Delete_User_Access(long i_user_id)
        {

            //  PCK_KPISCOREBOARD.Delete_User_Access(i_user_id => :i_user_id);

            Capture_Log("", "", i_user_id.ToString(), CheckCurrentIdentity,"" , "Delete User Access");

            var conn = new OracleConnection(OraConnectionString());

            var cmd = new OracleCommand("PCK_KPISCOREBOARD.Delete_User_Access", conn)
            {
                CommandType = CommandType.StoredProcedure,
                BindByName = true
            };

            OracleParameter cp = new OracleParameter
            {
                ParameterName = "i_user_id",
                Size = 200,
                OracleDbType = OracleDbType.Int64,
                Direction = ParameterDirection.Input,
                Value = i_user_id
            };
            cmd.Parameters.Add(cp);

            cmd.Connection.Open();
            cmd.ExecuteScalar();
            cmd.Connection.Close();
            cmd.Dispose();
        }
        public DataTable gv_DisplayUserAccess()
        {
            //PCK_KPISCOREBOARD.gv_Display_KpiScoreBoard

            var dt = new DataTable();
            var conn = new OracleConnection(OraConnectionString());

            conn.Open();
            var cmd = new OracleCommand("PCK_KPISCOREBOARD.gv_DisplayUserAccess", conn)
            {
                CommandType = CommandType.StoredProcedure,
                BindByName = true
            };

            OracleParameter cp1 = new OracleParameter
            {
                ParameterName = "result",
                OracleDbType = OracleDbType.RefCursor,
                Direction = ParameterDirection.ReturnValue,
                Size = 5000
            };

            cmd.Parameters.Add(cp1);

            var dataReader = cmd.ExecuteReader();
            dt.Load(dataReader);
            cmd.Connection.Close();
            cmd.Dispose();
            return dt;

        }
        public int Get_MonthNumber()
        {
            //PCK_KPISCOREBOARD.gv_Display_KpiScoreBoard

            var dt = new DataTable();
            var conn = new OracleConnection(OraConnectionString());

            conn.Open();
            var cmd = new OracleCommand("PCK_KPISCOREBOARD.Get_MonthNumber", conn)
            {
                CommandType = CommandType.StoredProcedure,
                BindByName = true
            };

            OracleParameter cp1 = new OracleParameter
            {
                ParameterName = "result",
                OracleDbType = OracleDbType.RefCursor,
                Direction = ParameterDirection.ReturnValue,
                Size = 5000
            };

            cmd.Parameters.Add(cp1);

            var dataReader = cmd.ExecuteReader();
            dt.Load(dataReader);
            cmd.Connection.Close();
            cmd.Dispose();

          var monthno = dt.Rows[0]["MONTH_TO_NUMBER"].ToString();

            return Convert.ToInt32(monthno);

        }

        public DataTable gv_DisplayLog()
        {
            //PCK_KPISCOREBOARD.gv_Display_KpiScoreBoard

            var dt = new DataTable();
            var conn = new OracleConnection(OraConnectionString());

            conn.Open();
            var cmd = new OracleCommand("PCK_KPISCOREBOARD.gv_DisplayLog", conn)
            {
                CommandType = CommandType.StoredProcedure,
                BindByName = true
            };

            OracleParameter cp1 = new OracleParameter
            {
                ParameterName = "result",
                OracleDbType = OracleDbType.RefCursor,
                Direction = ParameterDirection.ReturnValue,
                Size = 5000
            };

            cmd.Parameters.Add(cp1);

            var dataReader = cmd.ExecuteReader();
            dt.Load(dataReader);
            cmd.Connection.Close();
            cmd.Dispose();
            return dt;

        }
        //*******************LOG Changes*******************************
        public void Capture_Log(string i_group_id, string i_kpi_id, string i_user_id, string i_username, string i_details, string i_transaction)
        {
            //procedure Capture_Log(i_group_id in varchar2
            //          , i_kpi_id in varchar2
            //          , i_user_id in varchar2
            //          , i_username in varchar2
            //          , i_details in varchar2
            //          , i_transaction in varchar2) is

             var conn = new OracleConnection(OraConnectionString());
            //var cmd = new OracleCommand();

            conn.Open();

            var cmd = new OracleCommand("PCK_KPISCOREBOARD.Capture_Log", conn);

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.BindByName = true;


            OracleParameter cp1 = new OracleParameter
            {
                ParameterName = "i_group_id",
                Size = 200,
                OracleDbType = OracleDbType.Varchar2,
                Direction = ParameterDirection.Input,
                Value = i_group_id
            };
            cmd.Parameters.Add(cp1);
            OracleParameter cp2 = new OracleParameter
            {
                ParameterName = "i_kpi_id",
                Size = 200,
                OracleDbType = OracleDbType.Varchar2,
                Direction = ParameterDirection.Input,
                Value = i_kpi_id
            };
            cmd.Parameters.Add(cp2);
            OracleParameter cp3 = new OracleParameter
            {
                ParameterName = "i_username",
                Size = 200,
                OracleDbType = OracleDbType.Varchar2,
                Direction = ParameterDirection.Input,
                Value = i_username
            };
            cmd.Parameters.Add(cp3);
            OracleParameter cp4 = new OracleParameter
            {
                ParameterName = "i_details",
                Size = 200,
                OracleDbType = OracleDbType.Varchar2,
                Direction = ParameterDirection.Input,
                Value = i_details
            };
            cmd.Parameters.Add(cp4);
            OracleParameter cp5 = new OracleParameter
            {
                ParameterName = "i_transaction",
                Size = 200,
                OracleDbType = OracleDbType.Varchar2,
                Direction = ParameterDirection.Input,
                Value = i_transaction
            };
            cmd.Parameters.Add(cp5);

            OracleParameter cp6 = new OracleParameter
            {
                ParameterName = "i_user_id",
                Size = 200,
                OracleDbType = OracleDbType.Varchar2,
                Direction = ParameterDirection.Input,
                Value = i_user_id
            };
            cmd.Parameters.Add(cp6);


            cmd.ExecuteReader();
            cmd.Connection.Close();
            cmd.Dispose();

        }
  
        //*************************************************************

        public bool FindADUserDetails(string pUserName)
        {
            string test = "";
            bool check = false;
            try
            {
                using (var context = new PrincipalContext(ContextType.Domain, "Sappisa"))//, uid, pwd))
                {
                    using (UserPrincipal user = new UserPrincipal(context))
                    {
                        user.SamAccountName = pUserName;
                        using (var searcher = new PrincipalSearcher(user))
                        {
                            foreach (var result in searcher.FindAll())
                            {
                                DirectoryEntry de = result.GetUnderlyingObject() as DirectoryEntry;


                                check = true;
                                //nameAD = (de.Properties["samAccountName"].Value).ToString();
                                //displayNameAD = (de.Properties["displayName"].Value).ToString();
                                //emailAD = (de.Properties["mail"].Value).ToString();
                                ////  employeeidAD = (de.Properties["employeeID"].Value).ToString();
                                //contactAD = (de.Properties["telephonenumber"].Value).ToString();
                                test = (de.Properties["Description"].Value).ToString();
                                //  txtUserOffice.Text = (de.Properties["physicalDeliveryOfficeName"].Value).ToString().TrimEnd('.');
                               
                            }
                        }
                        if (test == "")
                        {

                            //   throw new System.ArgumentException("Unable To Find User");
                            check = false;

                        }

                      //  messageAD = "User Found";

                    }
                }
            }
            catch (Exception)
            {
                //throw;
            //    messageAD = "User Not Found";
            }

            return check;
        }
        //private string GetADDetails(string pUserName, string paramName)
        //{
        //    //string uid = Properties.Settings.Default.uid;
        //    //string pwd = Properties.Settings.Default.pwd;
        //    string email = "";
        //    try
        //    {


        //        using (var context = new PrincipalContext(ContextType.Domain, "Sappisa"))//, uid, pwd))
        //        {
        //            using (UserPrincipal user = new UserPrincipal(context))
        //            {
        //                user.SamAccountName = pUserName;
        //                using (var searcher = new PrincipalSearcher(user))
        //                {
        //                    foreach (var result in searcher.FindAll())
        //                    {
        //                        DirectoryEntry de = result.GetUnderlyingObject() as DirectoryEntry;

        //                        email = (de.Properties[paramName].Value).ToString();
        //                        //  txtUserFromAD.Text = (de.Properties["samAccountName"].Value).ToString();
        //                        // test = (de.Properties["Description"].Value).ToString();
        //                        //  txtUserOffice.Text = (de.Properties["physicalDeliveryOfficeName"].Value).ToString().TrimEnd('.');

        //                    }
        //                }
        //            }
        //        }

        //    }
        //    catch (Exception)
        //    {

        //        //   throw;
        //    }
        //    return email;
        //}
        public string OraConnectionString()
        {
            return Properties.Settings.Default.DataConnection;//+ "Statement Cache Size=1";

        }
    }
}
