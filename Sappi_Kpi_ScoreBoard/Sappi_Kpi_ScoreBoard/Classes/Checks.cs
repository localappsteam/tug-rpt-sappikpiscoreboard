﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;

namespace Sappi_Kpi_ScoreBoard.Classes
{
    public class Checks: Classes.DBBackend
    {
        //Classes.DBBackend DBBackend = new Classes.DBBackend();
        public double? checkIfNull(string txt)
        {
            double? val = null;
            try
            {
                if (String.IsNullOrWhiteSpace(txt))
                {
                }
                else
                {
                    double.TryParse(txt, out double val1);

                    val = val1;
                }

            }
            catch (Exception)
            {
                val = null;
            }
            return val;
        }

        public bool CheckActiveMonth(int monthnumber)
        {
            bool check = false;
            //  int _monthNumber = Convert.ToInt32(monthnumber);

            if (monthnumber == Get_MonthNumber())
                check = true;
            else
                check = false;

            return check;
        }


        public Color CheckStatusString(string target, string month, string upperlimitcolour, string lowerlimitcolour)
        {
            //Color lblSuccess = Color.Green; //"label label - success";
            //  Color lblDanger = Color.Red;//"label label-danger";
            Color lblAssign = Color.DarkBlue;

            Color UpperLimitColour = ColorTranslator.FromHtml(upperlimitcolour);
            Color LowerLimitColour = ColorTranslator.FromHtml(lowerlimitcolour);

            try
            {
                if (target != "")
                {
                    if (Convert.ToDouble(month) > Convert.ToDouble(target))
                        lblAssign = UpperLimitColour;
                    else
                        lblAssign = LowerLimitColour;
                }
            }
            catch (Exception)
            {

                //throw;
            }
            return lblAssign;
        }

        public string CheckCurrentIdentity()
        {
            // must exist in web.config
            // <authentication mode="Windows">
            // </authentication>
            //BaseConfig.sUserName = Page.User.Identity.Name.Replace("SAPPISA\\", "");
            //BaseConfig.sUserName = Thread.CurrentPrincipal.Identity.Name.Replace("SAPPISA\\", "");
            //BaseConfig.sUserName = Request.ServerVariables["LOGON_USER"].Replace("SAPPISA\\", "");
            string sUser = HttpContext.Current.User.Identity.Name.Replace("SAPPISA\\", "");
            //sUser = sUser.ToLower().Replace("adm-", string.Empty);
            //if (sUser.ToUpper() == "DDEARRUDA")
            //    sUser = "DArruda";
            ////sUser = "agreenway";

            return sUser.ToUpper().Trim();
        }

        public int checkAccessLevel()
        {
           return Get_User_Rights(CheckCurrentIdentity());
        }

        public bool CheckForAccess()
        {
            bool check = false;

            switch (checkAccessLevel())
            {
                case 0:
                    check = true;
                    break;

                case 1:
                    check = false;
                    break;

                case 2:
                    check = true;
                    break;

                default:
                    check = true;
                    break;
            }

            return check;
        }
    }
}